# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 11:14:33 2020

@author: Davis
"""


import visa
import numpy as np
import matplotlib.pyplot as plt

rm = visa.ResourceManager()

ahb = rm.open_resource('GPIB0::25::INSTR')
source = rm.open_resource('GPIB0::20::INSTR')

volts = np.linspace(0, 20, 20)
port = 'UM_v2'

plt.close('all')
fig, ax = plt.subplots(2,1)
ax[0].set_title('Capacitance')
ax[1].set_title('Loss')

Cs = []
Cse = []
Ls = []
Lse = []
for v in volts:
    print("Starting {} V".format(v))
    source.write(':SOUR:VOLT:LEV {:.4f}'.format(v))
    Csi = []
    Lsi = []
    k=0
    # for k in range(20):
    while True:
        resp = ahb.query('SINGLE')
        print(resp)
        Ck = float(resp.rsplit()[1])
        Lk = float(resp.rsplit()[4])
        if Lk > 1:
            continue
        Csi.append(Ck)
        Lsi.append(Lk)
        k+=1
        if k > 20:
            break
    print("C = {}, L = {}".format(np.mean(Csi), np.mean(Lsi)))
    Cs.append(np.mean(Csi))
    Cse.append(np.std(Csi))
    Ls.append(np.mean(Lsi))
    Lse.append(np.std(Lsi))

ax[0].errorbar(volts, Cs, yerr=Cse, fmt='-o')
ax[1].errorbar(volts, Ls, yerr=Lse, fmt='-o')


data = np.column_stack((volts, Cs, Cse, Ls, Lse))
np.savetxt('capacitance_{}_5VE.txt'.format(port), data)

source.write(':SOUR:VOLT:LEV 0')
source.close()
ahb.close()
rm.close()