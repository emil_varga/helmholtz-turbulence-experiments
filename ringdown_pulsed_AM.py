# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 11:52:39 2021

@author: Davis
"""


import numpy as np
import matplotlib.pyplot as plt
import os
import time
import visa

import sys
sys.path.append("D:\\ev")
from instruments.DAQcard import DAQcard
from instruments.KS33210A import KS33210A
from instruments.SR830 import SR830

import ltwsclient as ltwsc
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

data_dir = 'D:/ev/data/RUN20210104/dumped/450nm/driven/AM/power_dependence/Ccomp'

rm = visa.ResourceManager()
lockin = SR830(rm, 'GPIB0::1::INSTR')
gen = KS33210A(rm, '33210A')

timeconstant = '100u'
slope = '6'
sensitivity = '200m'
carrier_frequency = 31e3 #Hz
carrier_amplitude = 20 #Vpp
preamp_gain = 200e-6 #A/V SET BY HAND
modulation_depths = [20] #% of carrier amplitude

gen.frequency(carrier_frequency)
gen.amplitude(carrier_amplitude)
gen.amplitude_modulation(True, modulation_depths[0])
gen.output(True)

lockin.set_reference('external')
lockin.set_timeconstant(timeconstant)
lockin.set_slope(slope)
lockin.set_sensitivity(sensitivity)

DAQrate = 8192
pulse_t = 32 #s, total duration of the pulse
pulse_ti = 0.5 #s, start of the chrip
pulse_fi = 10 #Hz
pulse_tf = 16.5 #s, end of the chirp
pulse_ff = 1300 #Hz
pulse_tw = 1 #Rise/fall width
pulse_amplitude = 0.5


time_per_power = 2*60*60 #2 hours
max_T = 2.5 #don't measure above this temperature

DAQsamples = int(DAQrate*pulse_t)

def envelope_function(t, ti, tf, tw):
    tt = np.atleast_1d(t)
    left = np.exp(-(tw/(tt - ti))**2)
    right = np.exp(-(tw/(tt - tf))**2)
    env = left*right
    ix0 = np.logical_or(tt < ti, tt > tf)
    env[ix0] = 0
    return env

def chirped_pulse(t, ti, tf, tw, fmin, fmax):
    dt = tf - ti
    df = fmax - fmin
    phi = fmin*(t - ti) + 0.5*df*(t - ti)**2/dt
    chirp = np.sin(2*np.pi*phi)
    env = envelope_function(t, ti, tf, tw)
    return chirp*env


plt.close('all')

pulse_ts = np.linspace(0, pulse_t, pulse_t*DAQrate)
pulse = chirped_pulse(pulse_ts, pulse_ti, pulse_tf, pulse_tw, pulse_fi, pulse_ff)

pulse_ft = np.fft.rfft(pulse, n=DAQsamples)
fres = np.fft.rfftfreq(DAQsamples, 1/DAQrate)


try:
    daq = DAQcard(['ai0', 'ai1'], rate=DAQrate, samples=DAQsamples, min_val=-10, max_val=10,
                          outputs=('ao0', pulse*pulse_amplitude))
    while True:
        for md in modulation_depths:
            gen.amplitude_modulation(True, md)
            output_dir = os.path.join(data_dir, 'AMD_{:.3f}pc_{:.3f}V'.format(md, pulse_amplitude))
            os.makedirs(output_dir, exist_ok=True)
            start_time = time.time()
            while time.time() - start_time < time_per_power:
                Ti = lt.get_T3()
                if Ti > max_T:
                    time.sleep(1)
                    continue
                timestamp = time.strftime('%Y%m%d-%H%M%S')
                data = daq.write_measure()
                Tf = lt.get_T3()
                print(timestamp)
                output = {'timeseries': data, 'Ti': Ti, 'Tf': Tf, 'pulse': pulse,
                          'pulse_amplitude': pulse_amplitude,
                          'samples': DAQsamples, 'rate': DAQrate,
                          'carrier_amplitude': carrier_amplitude,
                          'carrier_frequency': carrier_frequency,
                          'lia_tc': timeconstant,
                          'lia_sens': sensitivity,
                          'lia_slope': slope,
                          'preamp_gain': preamp_gain,
                          'AMdepth': md}
                np.save(os.path.join(output_dir, 'DM_{}.npy'.format(timestamp)), output)
finally:
    daq.close()
    gen.output(False)
    gen.amplitude_modulation(False)
    gen.close()
    lockin.close()
    rm.close()