#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 17:01:02 2019

@author: emil
"""

import numpy as np
import ziLockin as zi
from datetime import datetime
import os.path as path
import os
import msvcrt
import ltwsclient as ltwsc
import time

device_id = 'dev538'
reps = 2
continuous = True

#output_dir = '../data/RUN_20200712/lockin_fsweeps/UM/T1000_hr2/'
#freq_i = 1600 #Hz
#freq_f = 1660 #Hz
#samples = 150
#time_constant = 500e-3
#T3max = 1.025
#As = np.linspace(0.1e-3, 100e-3, 50)

output_dir = '../data/RUN_20200712/lockin_fsweeps/UM/T1000_hr3_reboot/'
freq_i = 50e3 #Hz
freq_f = 125e3 #Hz
samples = 10000
time_constant = 100e-3
As = [1]
T3max = 0.5
#As = np.linspace(0.5e-3, 5e-3, 10)
#As = np.linspace(15e-3, 50e-3, 30)
#As = np.linspace(50e-3, 100e-3, 10)
# As = np.linspace(5e-3, 0.1, 10)

print(As)

os.makedirs(output_dir,exist_ok=True)

input_channel  = 0
output_channel = 6
output_id      = 0
osc            = 0
demod_id       = 0
sample_rate    = 200


#data readout from logger
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

def wait_for_T3(lt, cutoff_T3, sleep_time=5):
    print(f"Waiting for T3 to go below {T3max}.")
    attempts = 0
    while True:
        try:
            last_5min = lt.get_last_measurements('3hepot_t', 5*60)
            if len(last_5min) == 0:
                continue
            attempts = 0
        except:
            attempts+=1
            if attempts > 5:
                print('cant get data from logger')
                raise
            continue
        #check that the data are up-to-date
        if abs(time.time() - last_5min[-1][0]) > 600:
            raise RuntimeError('old data from logger')
        #there is some weird offset of about 80 s between the logger time and local time
        t = last_5min[:, 0]
        t0 = t[-1]
        T3 = last_5min[t > t0-10,1].mean() #average only over the last 10 s
        print(f"T3 is {T3}")
        if T3 < cutoff_T3:
            break
        time.sleep(sleep_time)
    return T3

def get_T3(lt):
    last_5min = lt.get_last_measurements('3hepot_t', 5*60)
    t = last_5min[:, 0]
    t0 = t[-1]
    T3 = last_5min[t > t0-30,1].mean() #average only over the last 30 s
    return T3

if __name__ == '__main__':
    print('Connecting to lockin.')
    lockin = zi.ziLockin(device_id)
    
    print('Configuring input.')
    lockin.configure_input(input_channel, input_range = 0.1,
                           ac_coupling=True, imp50 = False,
                           differential=False)
    demods = [0]
    harms = [1]
    
    print('Configuring demodulators.')
    for d, h in zip(demods,harms):
        lockin.configure_demodulator(d, sample_rate, input_channel = input_channel, filter_order=4, 
                                     tc = time_constant, osc=osc, harm=h)
    not_done = True
    try:
        while not_done:
            for r in range(reps):
                NA = len(As)
                kA = 0
                while kA < NA:
                    A = As[kA]
                    T3 = wait_for_T3(lt, T3max)
                    
                    print('Starting ', A, 'V')
                    now = datetime.now()
                    timestamp = now.strftime('%Y%m%d-%H_%M_%S')
                    sweep_data = lockin.freq_sweep(freq_i, freq_f, samples, A, osc, output_id, output_channel, demods = demods,
                                                   settling_time = 10*time_constant, settling_inaccuracy=10e-3,
                                                   avg_samples = 2, verbose=True, timeout=np.inf,
                                                   scan = zi.scan_reverse)
                    sweep_data['_drive_amplitude'] = A
                    sweep_data['_Tstart'] = T3
                    T3end = get_T3(lt)
                    sweep_data['_Tend'] = T3end
                    filename = 'FS-'+timestamp
                    np.save(path.join(output_dir, filename), sweep_data)
                    if sweep_data['_Tend'] < T3max+0.3:
                        kA += 1
            if not continuous:
                not_done = False
            else:
                if msvcrt.kbhit():
                    if msvcrt.getch() == 'q':
                        not_done = False
    finally:
        lockin.disable_everything()