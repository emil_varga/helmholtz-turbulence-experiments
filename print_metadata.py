# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 17:45:01 2021

@author: ev
"""

import numpy as np
import os.path as path
from glob import glob


dir05 = r'Z:/emil/data/He-4/data/RUN20210104/dumped/450nm/thermomechanics/lockin/feedback/T700/AM0.1/PHI_0.000_DEG/'
dir1 = r'Z:/emil/data/He-4/data/RUN20210104/dumped/450nm/thermomechanics/lockin/feedback/T700/AM0.5/PHI_30.000_DEG/'

file05 = glob(path.join(dir05, '*.npy'))[0]
file1 = glob(path.join(dir1, '*.npy'))[0]

d05 = np.load(file05, allow_pickle=True).item()
d1 = np.load(file1, allow_pickle=True).item()

for key in d05.keys():
    if key=='timeseries':
        continue
    
    print(key, d05[key], '|', d1[key])