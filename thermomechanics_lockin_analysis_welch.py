# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: emil
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig

from matplotlib.colors import LogNorm

import os
from glob import glob

plt.style.use('aps')
plt.close('all')

data_dir = r'Z:\emil\data\He-4\data\RUN20210104\dumped\450nm\thermomechanics\lockin\Tsweep_fast\10V'
# data_dir = r'D:/ev/data/RUN20210104/750nm/thermomechanics/lockin/KZM/10mKmin'

files = glob(os.path.join(data_dir, '*.npy'))
files.sort()
print(len(files))

nscale=1

lockin_channel = 0

data0 = np.load(files[0], allow_pickle=True).item()
z = data0['timeseries'][lockin_channel,:]
nperseg = data0['samples']/4
noverlap = int(nperseg/2)
nfft = data0['samples']
freqs, _ = sig.welch(z, fs=data0['rate'], nperseg=nperseg, noverlap=noverlap, nfft=nfft,
                     return_onesided=True)

Ts = np.linspace(2.25, 0.5, 600)
grid = np.zeros((len(Ts), len(freqs)))
samples = np.zeros_like(grid)

for file in files:
    data = np.load(file, allow_pickle=True).item()
    Ti = data['Ti']
    Tf = data['Tf']
    if abs(Ti - Tf) > 0.2:
        continue
    z = data['timeseries'][lockin_channel,:]
    _, resp = sig.welch(z, nperseg=nperseg, noverlap=noverlap, nfft=nfft,
                        return_onesided=True)
    # _, resp = sig.periodogram(z, fs=data['rate'])
    T = 0.5*(Ti + Tf)
    if not np.isnan(T):
        # ix = np.argmin(np.abs(Ts - T))
        ix = np.logical_and(Ts > min(Ti, Tf), Ts < max(Ti, Tf))
        Nix = sum(ix)
        if Nix == 0:
            # print("{} ({}-{}) Doesn't belong anywhere!".format(T, Ti, Tf))
            ix = np.argmin(np.abs(Ts - T))
            Nix = 1
        print(T, Nix, max(np.abs(data['timeseries'][lockin_channel,:])))
        grid[ix, :] += resp/Nix
        samples[ix, :] += 1.0/Nix
grid /= samples
# bg = np.nanmean(grid[Ts > 2.2, :], axis=0)
# grid[np.isnan(grid)] = 0
bg=1
fig, ax = plt.subplots(1, 1, figsize=(2.5, 2.5))
im = ax.imshow(grid, extent=[freqs.min(), freqs.max(), Ts.min(), Ts.max()], origin='upper',
               interpolation='nearest', norm=LogNorm(vmin=1e-5, vmax=100))
ax.set_xlabel('frequency (Hz)')
ax.set_ylabel('temperature (K)')
ax.set_xlim(760, 900)
ax.set_ylim(0.6, 1.6)
cbar = fig.colorbar(im)
cbar.set_label('$S_{xx}$ (V$^2$/Hz)')
fig.tight_layout()

fig, ax = plt.subplots(1, 1)
ix = np.logical_and(Ts < 0.801, Ts > 0.799)
y = np.nanmean(grid[ix, :], axis=0)
N = len(freqs)
N2 = int(N/2)
# ax.plot(freqs[N2:], y[N2:], '-o', ms=3)
# ax.plot(freqs[N2:], y[1:N2+1][::-1], '-o', ms=3)
ax.plot(freqs, y, '-o')

