# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: emil
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
from fano_peak import SingleFanoPeak

import os
from glob import glob

base_dir = 'Z:/emil/data/He-4/Helmholtz_turbulence/data/RUN20210104/dumped'\
           '/750nm/thermomechanics/lockin/KZM/Tramp/repeated/'

data_dir = r'Z:\emil\data\He-4\Helmholtz_turbulence\data\RUN20210104\dumped\750nm\driven\DAQ_battery\power_dependence\0\AMP_522mV'

plt.close('all')

def get_T(file):
    data = np.load(file, allow_pickle=True).item()
    return 0.5*(data['Ti'] + data['Tf'])

# rates = ['0.002', '0.05', '0.1', '0.5', '1']
# rates = ['0.05', '0.02', '0.01', '0.005', '0.002']
rates = ['0.02']

# thermomechanics, 800 mK
f1 = 100
f2 = 1250

#thermomechanics, 600 mK
# f1 = 1210
# f2 = 1214

# #drive, 800 mK
# f1 = 1125
# f2 = 1275

for r in rates:
    Trate = '{}Kmin'.format(r)
    print(Trate)
    # data_dir = os.path.join(base_dir, Trate)
    
    files = glob(os.path.join(data_dir, '*.npy'))
    files.sort(key=get_T)
    print(len(files))
    
    Ts = []
    fwhms = []
    p0p = None
    p0n = None
    for file in files[::5]:
        data = np.load(file, allow_pickle=True).item()
        Ti = data['Ti']
        Tf = data['Tf']
        T = 0.5*(Ti + Tf)
        # if abs(Tf - Ti) > 0.1:
        #     continue

        z = data['timeseries'][0,:] + 1j*data['timeseries'][1,:]
        nperseg = data['samples']/2
        noverlap = None#int(nperseg/2)
        nfft = None#data['samples']
        freqs, resp = sig.welch(z, fs=data['rate'],
                            nperseg=nperseg, noverlap=noverlap, nfft=nfft, return_onesided=False)
        # resp = np.sqrt(resp)
        try:
            ixp = np.logical_and(freqs > min(f1, f2), freqs < max(f1, f2))
            ixn = np.logical_and(freqs > min(-f1, -f2), freqs < max(-f1, -f2))
            peakp = SingleFanoPeak(freqs[ixp], resp[ixp], p0 = p0p)
            peakn = SingleFanoPeak(freqs[ixn], resp[ixn], plot=True, p0 = p0n)
            p0p = np.copy(peakp.popt)
            p0n = np.copy(peakn.popt)
            peakn.ax.set_title("{} K/min, {} K".format(r, T))
            peakn.fig.tight_layout()
            Ts.append(T)
            fwhms.append(0.5*(abs(peakp.fwhm) + abs(peakn.fwhm)))
        except RuntimeError:
            print("Plot failed.")

    fig, ax = plt.subplots(1, 1)
    ax.plot(Ts, fwhms,'o')
    ax.set_title(Trate)

