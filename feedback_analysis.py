# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 12:14:52 2021

@author: ev
"""

import numpy as np
import os.path as path
import matplotlib.pyplot as plt
import scipy.fft as fft

from psd_peak import SingleFanoPeak

from glob import glob

def get_phase(DEGdir):
    dname = path.split(DEGdir)[-1]
    phase = float(dname.split('_')[1])
    return phase

def get_AMdepth(AMdir):
    return float(path.split(AMdir)[-1][2:])

plt.close('all')

base_dir = 'Z:/emil/data/He-4/Helmholtz_turbulence/data/RUN20210104/dumped/450nm/thermomechanics/lockin/feedback'

peak_f1 = 840
peak_f2 = 880

AM_dirs = glob(path.join(base_dir, 'AM*'))
AM_dirs.sort(key=get_AMdepth)
figt, axt = plt.subplots(1, 1)
fig_pp, ax_pp = plt.subplots(2, 1, sharex=True) #peak parameters plots
for AMdir in AM_dirs[-1:]: #loop through all modulation depths
    print(path.split(AMdir)[-1])
    AMdepth = float(path.split(AMdir)[-1][2:])
    DEG_dirs = glob(path.join(AMdir, 'PHI*DEG'))
    if len(DEG_dirs) == 0:
        continue
    DEG_dirs.sort(key=get_phase)
    # fig, ax = plt.subplots(1, 1)
    # ax.set_title(path.split(AMdir)[-1])
    # ax.set_xlabel('frequency (Hz)')
    # ax.set_ylabel('Svv (a.u)')
    phases = []
    fwhms = []
    f0s = []
    p0=np.array([ 3.71920239e+01, -1.88458467e-02,  0.00000000e+00,  0.00000000e+00,
                  0.00000000e+00,  9.36473118e-02,  8.59998608e+02,  1.36461839e+00,
                  1.31254779e+91])
    for DEGdir in DEG_dirs: #loop through all feedback phases
        dname = path.split(DEGdir)[-1]
        phase = float(dname.split('_')[1])
        files = glob(path.join(DEGdir, 'FB*.npy'))
        print(dname, len(files))
        if len(files) == 0:
            continue
        avg_resp = 0
        for file in files: #loop through all files
            data = np.load(file, allow_pickle=True).item()
            z = data['timeseries'][0,:] + 1j*data['timeseries'][1,:]
            # axt.plot(np.real(z))
            ft1 = fft.fft(z-z.mean(), workers=4)
            avg_resp += np.abs(fft.fftshift(ft1))**2
        freqs = fft.fftshift(fft.fftfreq(data['samples'], 1/data['rate']))
        ix = freqs > min(peak_f1, peak_f2)
        ix &= freqs < max(peak_f1, peak_f2)
        try:
            peak = SingleFanoPeak(freqs[ix], avg_resp[ix], plot=False, p0=None)
            # p0=np.copy(peak.popt)
            # peak.ax.set_yscale('log')
            # ax.plot(freqs, avg_resp)
            phases.append(phase)
            fwhms.append(abs(peak.fwhm))
            f0s.append(abs(peak.f0))
        except RuntimeError:
            continue
    # fig.tight_layout()
    ax_pp[0].plot(phases, f0s, '-o', label="AM {}%".format(AMdepth))
    ax_pp[1].plot(phases, fwhms, '-o')
    ax_pp[0].set_ylabel('f0 (Hz)')
    ax_pp[1].set_ylabel('FWHM (Hz)')
    ax_pp[1].set_xlabel('phase (deg)')
ax_pp[0].legend(loc='best', ncol=2)
fig_pp.tight_layout()