# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: Davis
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime
from tqdm import tqdm

import matplotlib.pyplot as plt

from instruments.SR830 import SR830
from instruments.KS33210A import KS33210A
from ltwsclient import LTWebsockClient

server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = LTWebsockClient(server, port)

plot=False

output_dir = '../data/RUN_20200926/pump_modes/T1600/ampsweeps/1/10dB'

Vmin = 0.01
Vmax = 5
NV = 1000
amp_ranges = [(Vmin, Vmax, NV), (Vmax, Vmin, NV)]#, (Vmax, Vmin, NV)] #up, down, jump-down

lockin_timeconstant = '30m'
lockin_sensitivity = '100m'
lockin_slope = '12'
wait_time = 0.25

# freqs =[333, 2149, 2474.7] # 1.4 K
freqs = [315, 2033, 2342.5]
# freqs = [2474.7]

reps = 300

os.makedirs(output_dir,exist_ok=True)

rm = visa.ResourceManager()
lockin = SR830(rm, 'GPIB0::1::INSTR')
lockin.set_timeconstant(lockin_timeconstant)
lockin.set_sensitivity(lockin_sensitivity)
lockin.set_reference('external')
lockin.set_slope(lockin_slope)

generator = KS33210A(rm, '33210A')
generator.frequency(freqs[0])
# generator.amplitude(Vmin)
generator.output(False)

if plot:
    plt.close('all')
    fig, ax = plt.subplots(1,1)
i=0

def animate(i, ax, amps, xs, ys):
    rs = np.sqrt(np.array(xs)**2 + np.array(ys)**2)
    aa = np.array(amps)
    for line in ax.lines:
        line.set_zorder(2)
    if len(ax.lines) > i:
        ax.lines[i].set_xdata(amps)
        ax.lines[i].set_ydata(rs/aa)
        ax.lines[i].set_zorder(5)
    else:
        ax.plot(amps, rs/aa, '-o', ms=3, zorder=5)
    ax.relim()
    ax.autoscale_view()
    plt.pause(0.001)

try:
    for r in range(reps):
        for freq in freqs:
            generator.frequency(freq)
            time.sleep(1)
            for ar in amp_ranges:
                amps = np.linspace(ar[0], ar[1], ar[2])
                timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                xs, ys = [], []
                amps_r = []
                ts = []
                try:
                    T3i = lt.get_T3()
                except:
                    T3i = -1
                print("Starting {} V, {} Hz".format(ar, freq))
                # generator.amplitude(amps[0])
                lockin.set_output_amplitude(amps[0])
                time.sleep(5*wait_time)
                for amp in tqdm(amps):
                    # generator.amplitude(amp)
                    lockin.set_output_amplitude(amp)
                    time.sleep(wait_time)
                    # set_amp = float(generator.amplitude())
                    set_amp = lockin.get_output_amplitude()
                    x, y = lockin.get_xy()
                    amps_r.append(set_amp)
                    xs.append(x)
                    ys.append(y)
                    ts.append(time.time())
                    if plot:
                        animate(i, ax, amps_r, xs, ys)
                try:
                    T3f = lt.get_T3()
                except:
                    T3f = -1
                i+=1
                if i > 20:
                    i = 0
                data = np.column_stack((amps_r, xs, ys, ts))
                print(data.shape)
                out = {'frequency' : freq, 'data' : data,
                       'ampunits': 'Vrms',
                       'lockin_timeconstant': lockin_timeconstant,
                       'lockin_sensitivity': lockin_sensitivity,
                       'lockin_slope': lockin_slope,
                       'T3i': T3i,
                       'T3f': T3f}
                np.save(path.join(output_dir, 'AS_SR_'+timestamp), out, allow_pickle=True)
finally:
    generator.output(False)
    generator.close()
    lockin.close()
    rm.close()