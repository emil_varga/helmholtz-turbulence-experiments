# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: emil
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime
from tqdm import tqdm

import matplotlib.pyplot as plt

from instruments.SR830 import SR830
import instruments.ziLockin as ziLockin

from ltwsclient import LTWebsockClient

output_dir = '../data/RUN_20200926/pump_modes/T1400/probe_drive/1'
plot = False

lockin_sensitivity = '5m'
lockin_timeconstant = '30m'
lockin_slope = '24'
wait_time = 0.3


# probe fsweeps on the fundamental mode
probe_freqs = [(320, 345, 100, 0)]
probe_amplitudes = [0.05]

# drive fsweeps
drive_freqs = np.concatenate((np.linspace(500, 5000, 10),
                              np.linspace(2100, 2200, 25),
                              np.linspace(2350, 2600, 100)))
drive_amplitudes = np.linspace(1, 0, 10)

#finer 
# # probe fsweeps on the fundamental mode
# probe_freqs = [(320, 345, 200, 0)]
# probe_amplitudes = [0.05]

# # drive fsweeps
# drive_freqs = np.concatenate((np.linspace(500, 5000, 50),
#                               np.linspace(2100, 2200, 10),
#                               np.linspace(2350, 2600, 20)))
# drive_amplitudes = np.linspace(1, 0, 5)

# drive_freqs = [1000, 2149, 2472, 3000]
# drive_amplitudes = [0, 0.5, 1]

reps = 1

os.makedirs(output_dir,exist_ok=True)

rm = visa.ResourceManager()
lockin = SR830(rm, "GPIB0::1::INSTR")
lockin.set_timeconstant(lockin_timeconstant)
lockin.set_sensitivity(lockin_sensitivity)
lockin.set_slope(lockin_slope)
lockin.set_reference('internal')

HF2LI = ziLockin.ziLockin('dev538')
HF2LI.configure_input(input_id=0, input_range=2, ac_coupling=True, imp50=True, differential=False)
HF2LI.add_output(0, True)
HF2LI.configure_oscillator(osc_id=0, freq=1000)
HF2LI.configure_output(output_id=0, demod_id=0, amplitude=0)
HF2LI.output(output_id=0, output_on=True, output_range=10)

if plot:
    plt.close('all')
    fig, ax = plt.subplots(1, 1)
i = 0
zorder = 0

cmap = plt.get_cmap('viridis')

def animate(i, ax, fs, xs, ys, amp):
    rs = np.sqrt(np.array(xs)**2 + np.array(ys)**2)
    for line in ax.lines:
        line.set_zorder(2)
    if len(ax.lines) > i:
        ax.lines[i].set_xdata(fs)
        ax.lines[i].set_ydata(rs - rs.mean())
        ax.lines[i].set_zorder(5)
    else:
        ax.plot(fs, rs - rs.mean(), '-', ms=3, zorder=5)
    ax.relim()
    ax.autoscale_view()
    plt.pause(0.001)

try:
    server = "onnes.ccis.ualberta.ca"
    port = "3172"
    print(f"connecting to {server}:{port}")
    lt = LTWebsockClient(server, port)
    
    while True:
        for r in range(reps):
            for Ap in probe_amplitudes:
                for Ad in drive_amplitudes:
                    HF2LI.configure_output(output_id=0, demod_id=0, amplitude=Ad)
                    for fd in drive_freqs:
                        HF2LI.configure_oscillator(osc_id=0, freq=fd)
                        for fr in probe_freqs:
                            fs = np.linspace(fr[0], fr[1], fr[2])
                            timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                            xs, ys = [], []
                            fs_r = []
                            
                            print("Starting {} Hz, {} V; drive = {} Hz, {} V".format(fr, Ap, fd, Ad))
                            T3start = lt.get_T3()
                            lockin.set_output_amplitude(Ap)
                            lockin.set_frequency(fs[0]) # set the first frequency
                            time.sleep(2)
                            
                            for freq in tqdm(fs):
                                lockin.set_frequency(freq)
                                time.sleep(wait_time)
                                fset = lockin.get_frequency()
                                fset = float(fset)
                                x, y = lockin.get_xy()
                
                                fs_r.append(fset)
                                xs.append(x)
                                ys.append(y)
                                if plot:
                                    animate(i, ax, fs_r, xs, ys, Ap)
                                    plt.show()
        
                                
                            T3end = lt.get_T3()
                            
                            data = np.column_stack((np.array(fs_r), xs, ys))
                            print(data.shape)
                            out = {'probe_amplitude_Vrms' : Ap, 'data' : data, 'T3start' : T3start, 'T3end' : T3end,
                                   'timeconstant': lockin_timeconstant, 'sensitivity': lockin_sensitivity,
                                   'slope': lockin_slope,
                                   'wait_time': wait_time,
                                   'drive_amplitude_Vpp': Ad,
                                   'drive_frequency': fd}
                            np.save(path.join(output_dir, 'FS_SR_'+timestamp), out, allow_pickle=True)
                            i+=1
                            if i > 9:
                                i=0
        # break
finally:
    HF2LI.disable_everything()
    lockin.set_output_amplitude(0.01)
    lockin.close()
    rm.close()