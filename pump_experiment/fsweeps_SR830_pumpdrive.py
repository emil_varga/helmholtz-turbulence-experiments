# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: emil
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime
from tqdm import tqdm

import matplotlib.pyplot as plt

from instruments.SR830 import SR830
from instruments.KS33210A import KS33210A

from ltwsclient import LTWebsockClient

output_dir = '../data/RUN_20200926/pump_modes/T1800/LHP/nobias/init'
plot = True

lockin_sensitivity = '20u'
lockin_timeconstant = '100m'
harmonic = 3
wait_time = 0.5
auto_adjust_sensitivity=False

freqs = [(750, 1250, 300, 0)]
# freqs = [(1500, 2500, 300, 0)]
probe_amps = [5]
sensitivities = ['200u']
# probe_amps = [0.05, 0.5, 1, 2]
# sensitivities = ['20u', '100u', '100u', '1m']

# pump_amplitudes=[0.02, 20, 10, 5, 0.02]
pump_amplitudes=[20]
# pump_freqs = [50, 100, 297, 505]
pump_freqs = [149]
reps = 1

os.makedirs(output_dir,exist_ok=True)

rm = visa.ResourceManager()
lockin = SR830(rm, "GPIB0::1::INSTR")
lockin.set_timeconstant(lockin_timeconstant)
lockin.set_sensitivity(lockin_sensitivity)
lockin.set_reference('internal')
lockin.set_slope('12')
lockin.set_output_amplitude(probe_amps[0])
lockin.harmonic(harmonic)

generator = KS33210A(rm, '33210A')
generator.amplitude(pump_amplitudes[0])
generator.frequency(pump_freqs[0])
# generator.output(True)

if plot:
    plt.close('all')
    fig, ax = plt.subplots(1, 1)
i = 0
zorder = 0

cmap = plt.get_cmap('viridis')

def animate(i, ax, fs, xs, ys, amp):
    rs = np.sqrt(np.array(xs)**2 + np.array(ys)**2)
    for line in ax.lines:
        line.set_zorder(2)
    if len(ax.lines) > i:
        ax.lines[i].set_xdata(fs)
        ax.lines[i].set_ydata(rs)
        ax.lines[i].set_zorder(5)
    else:
        ax.plot(fs, rs, '-', ms=3, zorder=5)
    ax.relim()
    ax.autoscale_view()
    plt.pause(0.001)

try:
    server = "onnes.ccis.ualberta.ca"
    port = "3172"
    print(f"connecting to {server}:{port}")
    lt = LTWebsockClient(server, port)
    
    while True:
        for r in range(reps):
            for probeA, sens in zip(probe_amps, sensitivities):
                for pumpfreq in pump_freqs:
                    for pumpA in pump_amplitudes:
                        for fr in freqs:
                            fs = np.linspace(fr[0], fr[1], fr[2])
                            timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                            xs, ys = [], []
                            fs_r = []
                            
                            lockin.set_sensitivity(sens)
                            print("Starting {} Hz, pump {} V, {} Hz, probe {} V".format(fr, pumpA, pumpfreq,
                                                                                        probeA))
                            T3start = lt.get_T3()
                            lockin.set_output_amplitude(probeA)
                            lockin.set_frequency(fs[0]) # set the first frequency
                            generator.amplitude(pumpA)
                            generator.frequency(pumpfreq)
                            time.sleep(5)
                            
                            for freq in tqdm(fs):
                                lockin.set_frequency(freq)
                                time.sleep(wait_time)
                                fset = lockin.get_frequency()
                                fset = float(fset)
                                x, y = lockin.get_xy()
                                fs_r.append(fset)
                                xs.append(x)
                                ys.append(y)
                                if plot:
                                    animate(i, ax, fs_r, xs, ys, probeA)
                                    plt.show()
                                
                            T3end = lt.get_T3()
                            
                            data = np.column_stack((np.array(fs_r), xs, ys))
                            out = {'probe_amplitude_Vrms' : probeA, 'data' : data,
                                   'T3start' : T3start, 'T3end' : T3end,
                                    'timeconstant': lockin_timeconstant, 'sensitivity': sens,
                                    'lockin_harmonic': harmonic,
                                    'wait_time': wait_time,
                                    'pump_amplitude_Vpp': pumpA,
                                    'pump_frequency': pumpfreq}
                            np.save(path.join(output_dir, 'pumpprobe_FS_'+timestamp), out, allow_pickle=True)
                            i+=1
                            if i > 9:
                                i=0
finally:
    lockin.set_output_amplitude(0.01)
    lockin.set_sensitivity('1')
    lockin.close()
    generator.amplitude(0.02)
    generator.output(False)
    generator.close()
    rm.close()