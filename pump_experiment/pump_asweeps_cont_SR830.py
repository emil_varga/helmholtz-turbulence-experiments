# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: Davis
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime

import matplotlib.pyplot as plt

from instruments.SR830 import SR830
from instruments.SG384 import SG384

from ltwsclient import LTWebsockClient

output_dir = '../data/RUN_20200926/pumping/T1410/asweeps/1'

frequencies = [2145, 2475, 2300, 1900, 2700]
amplitudes = [(0.003, 2.8, 100), (2.8, 0.003, 100)]
wait_time = 1 # s

probe_frequency = 50e3
probe_amplitude = 1 # Vrms
reps = 1

lockin_timeconstant = '100m'
lockin_sensitivity = '50u'


os.makedirs(output_dir,exist_ok=True)

rm = visa.ResourceManager()

lockin = SR830(rm, "GPIB0::1::INSTR") # SR830
lockin.set_timeconstant(lockin_timeconstant)
lockin.set_sensitivity(lockin_sensitivity)
lockin.set_reference('internal')
lockin.set_frequency(probe_frequency)
lockin.set_output_amplitude(probe_amplitude)


pump_generator = SG384(rm, 'GPIB0::3::INSTR') # SG384
pump_generator.enableRF(False)
pump_generator.enableLF(True)
pump_generator.BNCamp(amplitudes[0][0])

# plt.close('all')
# fig, ax = plt.subplots(1, 1)
i = 0
zorder = 0

cmap = plt.get_cmap('viridis')

def animate(i, ax, fs, xs, ys, amp):
    rs = np.sqrt(np.array(xs)**2 + np.array(ys)**2)
    for line in ax.lines:
        line.set_zorder(2)
    if len(ax.lines) > i:
        ax.lines[i].set_xdata(fs)
        ax.lines[i].set_ydata(rs)
        ax.lines[i].set_zorder(5)
    else:
        ax.plot(fs, rs, '-', ms=3, zorder=5)
    ax.relim()
    ax.autoscale_view()
    plt.pause(0.001)

try:
    server = "onnes.ccis.ualberta.ca"
    port = "3172"
    print(f"connecting to {server}:{port}")
    lt = LTWebsockClient(server, port)
    
    while True:
        for r in range(reps):
            for f0 in frequencies:
                for Ai, Af, N in amplitudes:
                    As = np.linspace(Ai, Af, N)
                    xs, ys = [], []
                    As_r = []
                    ts = []
                    change_idxs = [0]
                    
                    print("Starting {} Hz, {} -- {} V".format(f0, Ai, Af))
                    start_timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                    T3start = lt.get_T3()
                    pump_generator.BNCamp(Ai) #sine out rms amplitude / V
                    aset = Ai
                    pump_generator.frequency(f0) #set the first frequency
                    time.sleep(5*wait_time)
                    t0 = time.time()
                    t_last_change = t0
                    aidx = 1
                    ptix = 0
                    ngot = 0
                    xm = 0
                    ym = 0
                    while True:
                        t = time.time()
                        if t - t_last_change > wait_time:
                            print("Got {} (x: {}, y: {})".format(ngot, xm/ngot, ym/ngot))
                            ngot = 0
                            xm = 0
                            ym = 0
                            pump_generator.BNCamp(As[aidx])
                            aset = pump_generator.BNCamp()
                            aset = float(aset)
                            t_last_change = t
                            aidx += 1
                            print("Switching to {} ({}/{}) V".format(aset, aidx, len(As)))
                            if aidx == len(As):
                                break
                            change_idxs.append(ptix)
    
                        x, y = lockin.get_xy()
        
                        ts.append(t)
                        As_r.append(aset)
                        xs.append(x)
                        ys.append(y)
                        # animate(i, ax, fs_r, xs, ys, A)
                        # plt.show()
                        ptix += 1
                        ngot += 1
                        xm += x
                        ym += y
                    T3end = lt.get_T3()
                    end_timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                    
                    data = np.column_stack((np.array(As_r), xs, ys, ts))
                    print(data.shape)
                    out = {'pump_frequency' : f0, 'data' : data, 'T3start' : T3start, 'T3end' : T3end,
                           'probe_amplitude_Vrms' : probe_amplitude, 'probe_frequency' : probe_frequency,
                           'start_timestamp' : start_timestamp,
                           'end_timestamp' : end_timestamp,
                           'lockin_sensitivity' : lockin_sensitivity,
                           'lockin_timeconstant' : lockin_timeconstant,
                           'wait_time' : wait_time,
                           'change_idxs' : change_idxs}
                    np.save(path.join(output_dir, 'pump_AS_SR_'+start_timestamp), out, allow_pickle=True)
                    i+=1
                    if i > 3:
                        i=0
        # break
finally:
    lockin.set_output_amplitude(0.01)
    pump_generator.enableLF(False)
    lockin.close()
    pump_generator.close()
    rm.close()