# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: Davis
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime

import matplotlib.pyplot as plt

from instruments.SR830 import SR830
from instruments.KS33210A import KS33210A

from ltwsclient import LTWebsockClient

output_dir = '../data/RUN_20200926/pumping/T1400/7/'

plot=False

# pump frequencies
freqs = [
    (2010, 2265, 300, 0),
    (2350, 2600, 300, 0)
    ]
# amplitudes = [10]
# amplitudes = np.linspace(1, 0.02, 5)
# amplitudes = np.concatenate((#np.linspace(20, 10, 5),
#                               np.linspace(6.25, 5, 3),
#                               np.linspace(5, 1, 5),
#                               np.linspace(1, 0.02, 5),
#                               np.linspace(1, 0.02, 5),
#                               np.linspace(1, 0.02, 5)))

# amplitudes = np.concatenate(([5], np.linspace(1, 0.02, 20)))
amplitudes = 2*np.logspace(1, -1, 30)
wait_time = 0.5 # s

#lowfreq
# output_dir = '../data/RUN_20200926/pumping/T2000/1'
# freqs = [(1, 2500, 500, 0)]
# amplitudes = np.linspace(0.01, 3, 5)
# wait_time = 5

# initial testing
# freqs = [(100, 3000, 500, 0)]
# amplitudes = [2]
# wait_time = 0.5

probe_frequency = 50e3
probe_amplitude = 5 # Vrms

reps = 1

lockin_timeconstant = '30m'
lockin_sensitivity = '200u'
lockin_slope = '24'

os.makedirs(output_dir,exist_ok=True)

rm = visa.ResourceManager()

lockin = SR830(rm, "GPIB0::1::INSTR") # SR830
lockin.set_timeconstant(lockin_timeconstant)
lockin.set_sensitivity(lockin_sensitivity)
lockin.set_reference('internal')
lockin.set_frequency(probe_frequency)
lockin.set_output_amplitude(probe_amplitude)
lockin.set_slope(lockin_slope)


pump_generator = KS33210A(rm, '33210A') # Keysight 33210A
pump_generator.output(True)
pump_generator.amplitude(amplitudes[0])

if plot:
    plt.close('all')
    fig, ax = plt.subplots(1, 1)
i = 0
zorder = 0

cmap = plt.get_cmap('viridis')

def animate(i, ax, fs, xs, ys, amp):
    rs = np.sqrt(np.array(xs)**2 + np.array(ys)**2)
    for line in ax.lines:
        line.set_zorder(2)
    if len(ax.lines) > i:
        ax.lines[i].set_xdata(fs)
        ax.lines[i].set_ydata(ys)
        ax.lines[i].set_zorder(5)
    else:
        ax.plot(fs, ys, '-', ms=3, zorder=5)
    ax.relim()
    ax.autoscale_view()
    plt.pause(0.001)

try:
    server = "onnes.ccis.ualberta.ca"
    port = "3172"
    print(f"connecting to {server}:{port}")
    lt = LTWebsockClient(server, port)
    
    while True:
        for r in range(reps):
            for A in amplitudes:
                for fr in freqs:
                    fs = np.linspace(fr[0], fr[1], fr[2])
                    xs, ys = [], []
                    fs_r = []
                    ts = []
                    change_idxs = [0]
                    
                    print("Starting {} Hz, {} V".format(fr, A))
                    start_timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                    try:
                        T3start = lt.get_T3()
                    except: 
                        T3start = -1
                    pump_generator.amplitude(A) #sine out rms amplitude / V
                    pump_generator.frequency(fs[0]) #set the first frequency
                    fset = fs[0]
                    time.sleep(5*wait_time)
                    t0 = time.time()
                    t_last_change = t0
                    fidx = 1
                    ptix = 0
                    ngot = 0
                    xm = 0
                    ym = 0
                    while True:
                        t = time.time()
                        if t - t_last_change > wait_time:
                            print("Got {} (x: {}, y: {})".format(ngot, xm/ngot, ym/ngot))
                            ngot = 0
                            xm = 0
                            ym = 0
                            pump_generator.frequency(fs[fidx])
                            fset = pump_generator.frequency()
                            fset = float(fset)
                            t_last_change = t
                            fidx += 1
                            print("Switching to {} ({}/{}) Hz".format(fset, fidx, len(fs)))
                            if fidx == len(fs):
                                break
                            change_idxs.append(ptix)
    
                        x, y = lockin.get_xy()
        
                        ts.append(t)
                        fs_r.append(fset)
                        xs.append(x)
                        ys.append(y)
                        if plot:
                            animate(i, ax, fs_r, xs, ys, A)
                            plt.show()
                        ptix += 1
                        ngot += 1
                        xm += x
                        ym += y
                    try:
                        T3end = lt.get_T3()
                    except:
                        T3end = -1
                    end_timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                    
                    data = np.column_stack((np.array(fs_r), xs, ys, ts))
                    print(data.shape)
                    out = {'pump_amplitude_Vpp' : A, 'data' : data, 'T3start' : T3start, 'T3end' : T3end,
                           'probe_amplitude_Vrms' : probe_amplitude, 'probe_frequency' : probe_frequency,
                           'start_timestamp' : start_timestamp,
                           'end_timestamp' : end_timestamp,
                           'lockin_sensitivity' : lockin_sensitivity,
                           'lockin_timeconstant' : lockin_timeconstant,
                           'lockin_slope' : lockin_slope,
                           'wait_time' : wait_time,
                           'change_idxs' : change_idxs}
                    np.save(path.join(output_dir, 'pump_FS_SR_'+start_timestamp), out, allow_pickle=True)
                    i+=1
                    if i > 3:
                        i=0
        # break
finally:
    print("Shutting down.")
    lockin.set_output_amplitude(0.01)
    pump_generator.amplitude(0.02)
    lockin.close()
    pump_generator.close()
    rm.close()