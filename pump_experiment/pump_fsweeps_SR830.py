# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: Davis
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime
from tqdm import tqdm

import matplotlib.pyplot as plt

from instruments.SR830 import SR830
from instruments.SG384 import SG384

from ltwsclient import LTWebsockClient

output_dir = '../data/RUN_20200926/pumping/T1500/2'

# pump frequencies
fL = 1 # Hz
fH = 3e3 # Hz
N = 300 # samples

probe_frequency = 50e3
probe_amplitude = 0.1 # Vrms
# amplitudes = [0, 0.1, 0.5, 1] # Vpp
amplitudes = [0.2, 0.5]
reps = 1

lockin_timeconstant = '100m'
lockin_sensitivity = '2m'
wait_time = 0.3 # s

freqs = [(fL, fH, N, 0), (fH, fL, N, 0)]

os.makedirs(output_dir,exist_ok=True)

rm = visa.ResourceManager()

lockin = SR830(rm, "GPIB0::1::INSTR") # SR830
lockin.set_timeconstant(lockin_timeconstant)
lockin.set_sensitivity(lockin_sensitivity)
lockin.set_reference('internal')
lockin.set_frequency(probe_frequency)
lockin.set_output_amplitude(probe_amplitude)


pump_generator = SG384(rm, 'GPIB0::3::INSTR') # SG384
pump_generator.enableRF(False)
pump_generator.enableLF(True)
pump_generator.BNCamp(amplitudes[0])

plt.close('all')
fig, ax = plt.subplots(1, 1)
i = 0
zorder = 0

cmap = plt.get_cmap('viridis')

def animate(i, ax, fs, xs, ys, amp):
    rs = np.sqrt(np.array(xs)**2 + np.array(ys)**2)
    for line in ax.lines:
        line.set_zorder(2)
    if len(ax.lines) > i:
        ax.lines[i].set_xdata(fs)
        ax.lines[i].set_ydata(rs)
        ax.lines[i].set_zorder(5)
    else:
        ax.plot(fs, rs, '-', ms=3, zorder=5)
    ax.relim()
    ax.autoscale_view()
    plt.pause(0.001)

try:
    server = "onnes.ccis.ualberta.ca"
    port = "3172"
    print(f"connecting to {server}:{port}")
    lt = LTWebsockClient(server, port)
    
    while True:
        for r in range(reps):
            for A in amplitudes:
                for fr in freqs:
                    fs = np.linspace(fr[0], fr[1], fr[2])
                    xs, ys = [], []
                    fs_r = []
                    
                    print("Starting {} Hz, {} V".format(fr, A))
                    start_timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                    T3start = lt.get_T3()
                    pump_generator.BNCamp(A) #sine out rms amplitude / V
                    pump_generator.frequency(fs[0]) #set the first frequency
                    time.sleep(2)
                    for freq in tqdm(fs):
                        pump_generator.frequency(freq)
                        fset = pump_generator.frequency()
                        fset = float(fset)
                        time.sleep(wait_time)
                        x, y = lockin.get_xy()
        
                        fs_r.append(fset)
                        xs.append(x)
                        ys.append(y)
                        animate(i, ax, fs_r, xs, ys, A)
                        plt.show()
                    T3end = lt.get_T3()
                    end_timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                    
                    data = np.column_stack((np.array(fs_r), xs, ys))
                    print(data.shape)
                    out = {'pump_amplitude_Vpp' : A, 'data' : data, 'T3start' : T3start, 'T3end' : T3end,
                           'probe_amplitude_Vrms' : probe_amplitude, 'probe_frequency' : probe_frequency,
                           'start_timestamp' : start_timestamp,
                           'end_timestamp' : end_timestamp,
                           'lockin_sensitivity' : lockin_sensitivity,
                           'lockin_timeconstant' : lockin_timeconstant,
                           'wait_time' : wait_time}
                    np.save(path.join(output_dir, 'pump_FS_SR_'+start_timestamp), out, allow_pickle=True)
                    i+=1
                    if i > 9:
                        i=0
        # break
finally:
    lockin.set_output_amplitude(0.01)
    pump_generator.enableLF(False)
    lockin.close()
    pump_generator.close()
    rm.close()