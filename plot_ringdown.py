# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 16:46:38 2019

@author: Davis
"""

import numpy as np
import matplotlib.pyplot as plt

import os.path as path
from glob import glob
import scipy.signal as signal

plt.close('all')

def get_T(file):
    d = np.load(file, allow_pickle=True).item()
    return 0.5*(d['Ti'] + d['Tf'])

data_dir = r'D:\ev\data\RUN20210104\all\driven\Tsweep\test\AMP_500mV'
files = glob(path.join(data_dir, 'DM*.npy'))
files.sort(key=get_T)

file = files[0]
print(get_T(file))
d0 = np.load(file, allow_pickle=True).item()
samples = d0['samples']
rate = d0['rate']
freq = np.fft.rfftfreq(samples, 1/rate)

pulse = d0['pulse']
pulse_ft = np.fft.rfft(pulse, n=samples)
phase = np.angle(pulse_ft)
avg_resp = 0
for file in files:
    d = np.load(file, allow_pickle=True).item()
    resp_ft = np.fft.rfft(d['timeseries'])/pulse_ft
    avg_resp += abs(resp_ft)

fig, ax = plt.subplots(1, 1)
ix = freq > 350
ix &= freq < 2000
ax.semilogy(freq[ix], avg_resp[ix])