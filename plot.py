# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 15:52:55 2021

@author: Davis
"""


import numpy as np
import matplotlib.pyplot as plt
from fano_peak import SingleFanoPeak


# plt.close('all')

# d2 = np.loadtxt('Base_2000mV_KZM2.txt')
# d10 = np.loadtxt('Base_2000mV_KZM10.txt')
# d20 = np.loadtxt('Base_2000mV_KZM20.txt')

# dx = np.loadtxt('KZMx_600mK.txt')
dp100 = np.loadtxt('wKZM1pps_600mK.txt')
dp20 = np.loadtxt('wKZM0.2pps_600mK.txt')
dp10 = np.loadtxt('wKZM0.1pps_600mK.txt')
dp5 = np.loadtxt('wKZM0.05pps_600mK.txt')
dp1 = np.loadtxt('wKZM0.01pps_600mK.txt')
dp05 = np.loadtxt('wKZM0.005pps_600mK.txt')
dp03 = np.loadtxt('wKZM0.003pps_600mK.txt')

fig, ax = plt.subplots(1, 1)
# ax.plot(d2[:,0], d2[:,1], '-s', ms=2, label='2')
# ax.plot(d10[:,0], d10[:,1], '-s', ms=2, label='10')
# ax.plot(d20[:,0], d20[:,1], '-s', ms=2, label='20')
# ax.plot(dx[:,0], dx[:,1], '-s', ms=2, label='x')

ax.plot(dp03[:,0], dp03[:,1], '-s', ms=2, label='p03')
ax.plot(dp05[:,0], dp05[:,1], '-s', ms=2, label='p05')
ax.plot(dp1[:,0], dp1[:,1], '-s', ms=2, label='p1')
ax.plot(dp5[:,0], dp5[:,1], '-s', ms=2, label='p5')
ax.plot(dp10[:,0], dp10[:,1], '-s', ms=2, label='p10')
ax.plot(dp20[:,0], dp20[:,1], '-s', ms=2, label='p20')
ax.plot(dp100[:,0], dp100[:,1], '-s', ms=2, label='p100')
ax.legend(loc='best')