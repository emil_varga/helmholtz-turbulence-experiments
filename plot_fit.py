# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 15:52:55 2021

@author: Davis
"""


import numpy as np
import matplotlib.pyplot as plt
from fano_peak import SingleFanoPeak
import os.path as path

plt.close('all')

def fit(file, f1, f2):
    d = np.loadtxt(file)
    f = d[:,0]
    x = d[:,1]
    ix = np.logical_and(f > min(f1, f2), f < max(f1, f2))
    peak = SingleFanoPeak(f[ix], x[ix], plot=False)
    peak.fit()
    basename = path.split(file)[-1]
    rate = float(basename.split('_')[0][4:-3])
    return rate, abs(peak.Q), abs(peak.A0), abs(peak.fwhm), abs(peak.f0)
    

files = ['rKZM1pps_600mK.txt',
         'rKZM0.5pps_600mK.txt',
         'rKZM0.1pps_600mK.txt',
         'rKZM0.05pps_600mK.txt',
         'rKZM0.002pps_600mK.txt']

rates = []
Qs = []
A0s = []
FWs = []
f0s = []

for file in files:
    rate, Qn, A0n, FWn, f0n = fit(file, -1210, -1214)
    _, Qp, A0p, FWp, f0p = fit(file, 1210, 1214)
    rates.append(rate)
    Qs.append(0.5*(Qn + Qp))
    A0s.append(0.5*(A0n + A0p))
    FWs.append(0.5*(FWn + FWp))
    f0s.append(0.5*(f0n + f0p))

fig, ax = plt.subplots(2, 2, sharex=True)
ax[0,0].plot(rates, Qs, 'o')
ax[0,1].plot(rates, A0s, 'o')
ax[1,0].plot(rates, FWs, 'o')
ax[1,1].plot(rates, f0s, 'o')
ax[1,0].set_xlabel('ramp rate (a.u.)')
ax[1,1].set_xlabel('ramp rate (a.u.)')
ax[0,0].set_ylabel('peak Q')
ax[0,1].set_ylabel('peak amplitude')
ax[1,0].set_ylabel('peak width (Hz)')
ax[1,1].set_ylabel('peak frequency (Hz)')
ax[0,0].set_xscale('log')
fig.tight_layout()

# p1 = np.polyfit(rates[1:], Qs[1:], deg=1)
# ax[0,0].plot(rates, np.polyval(p1, rates))

# d2 = np.loadtxt('Base_2000mV_KZM2.txt')
# d10 = np.loadtxt('Base_2000mV_KZM10.txt')
# d20 = np.loadtxt('Base_2000mV_KZM20.txt')

# dx = np.loadtxt('KZMx_600mK.txt')
# dp100 = np.loadtxt('wKZM1pps_600mK.txt')
# dp20 = np.loadtxt('wKZM0.2pps_600mK.txt')
# dp10 = np.loadtxt('wKZM0.1pps_600mK.txt')
# dp5 = np.loadtxt('wKZM0.05pps_600mK.txt')
# dp1 = np.loadtxt('wKZM0.01pps_600mK.txt')
# dp05 = np.loadtxt('wKZM0.005pps_600mK.txt')
# dp03 = np.loadtxt('wKZM0.003pps_600mK.txt')

# fig, ax = plt.subplots(1, 1)
# # ax.plot(d2[:,0], d2[:,1], '-s', ms=2, label='2')
# # ax.plot(d10[:,0], d10[:,1], '-s', ms=2, label='10')
# # ax.plot(d20[:,0], d20[:,1], '-s', ms=2, label='20')
# # ax.plot(dx[:,0], dx[:,1], '-s', ms=2, label='x')

# ax.plot(dp03[:,0], dp03[:,1], '-s', ms=2, label='p03')
# ax.plot(dp05[:,0], dp05[:,1], '-s', ms=2, label='p05')
# ax.plot(dp1[:,0], dp1[:,1], '-s', ms=2, label='p1')
# ax.plot(dp5[:,0], dp5[:,1], '-s', ms=2, label='p5')
# ax.plot(dp10[:,0], dp10[:,1], '-s', ms=2, label='p10')
# ax.plot(dp20[:,0], dp20[:,1], '-s', ms=2, label='p20')
# ax.plot(dp100[:,0], dp100[:,1], '-s', ms=2, label='p100')
# ax.legend(loc='best')