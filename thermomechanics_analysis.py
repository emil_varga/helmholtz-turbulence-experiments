# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: Davis
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

import os
from glob import glob

# data_dir =  '../../data/RUN20210104/all/thermomechanics/Tsweep/2'
# data_dir = '../../data/RUN20210104/all/thermomechanics/ramps/10mKmin'
# data_dir = '../../data/RUN20210104/all/thermomechanics/ramps/2.5mKmin'
# data_dir = '../../data/RUN20210104/all/thermomechanics/ramps/fast'
data_dir = r'D:\ev\data\RUN20210104\all\driven\Tsweep\0\AMP_571mV'

files = glob(os.path.join(data_dir, '*.npy'))
files.sort()
print(len(files))

data0 = np.load(files[0], allow_pickle=True).item()
freqs = np.fft.rfftfreq(data0['samples'], 1/data0['rate'])
Tt = data0['samples']/data0['rate']

Ts = np.linspace(2.25, 0.45, 1000)
grid = np.zeros((len(Ts), len(freqs)))
samples = np.zeros_like(grid)

for file in files:
    data = np.load(file, allow_pickle=True).item()
    resp = np.fft.rfft(data['timeseries'])/np.sqrt(Tt)
    if 'pulse' in data:
        pulse_ft = np.fft.rfft(data['pulse'], n=data['samples'])
        resp /= pulse_ft/abs(pulse_ft)
    Ti = data['Ti']
    Tf = data['Tf']
    T = 0.5*(Ti + Tf)
    if not np.isnan(T):
        # ix = np.argmin(np.abs(Ts - T))
        ix = np.logical_and(Ts > min(Ti, Tf), Ts < max(Ti, Tf))
        N = sum(ix)
        if N == 0:
            # print("{} ({}-{}) Doesn't belong anywhere!".format(T, Ti, Tf))
            ix = np.argmin(np.abs(Ts - T))
            N = 1
        # print(T, N, max(np.abs(data['timeseries'])))
        grid[ix, :] += np.abs(resp)/N
        samples[ix, :] += 1.0/N
grid /= samples

# plt.close('all')
fig, ax = plt.subplots(1, 1)
ax.imshow(np.log(grid), aspect='auto', extent=[freqs.min(), freqs.max(), Ts.min(), Ts.max()],
          interpolation='nearest')#, vmin=-3, vmax=3)
ax.set_xlabel('frequency (Hz)')
ax.set_ylabel('temperature (K)')

fig, ax = plt.subplots(1, 1)
y = np.nanmean(grid[Ts < 0.52,:], axis=0)
bg = np.nanmean(grid[Ts > 2.2, :], axis=0)
ax.plot(freqs, y-bg, '-o', ms=3)
np.savetxt("Base_driven.txt", np.c_[freqs, y-bg])
