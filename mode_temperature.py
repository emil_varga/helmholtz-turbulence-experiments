# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 17:20:23 2021

@author: ev
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as intp

import lmfit as lm

from os import path
from glob import glob

from helmholtz_calibration import HelmholtzF0Calibration

plt.close('all')

calibration = HelmholtzF0Calibration("TVcf0_450nm_calibration.npy")

Omega_c = 31e3*2*np.pi
tauL = 100e-6 #lockin time constant
tauH = 27e-6 #the DC block and AM input resistance

def feedback_f0(phase, f0, G, tauH):
    w0 = 2*np.pi*f0
    phi = phase/180*np.pi
    shift = Omega_c*w0**2*(tauL + tauH)*np.cos(phi) - w0**2*(1 - w0**2*tauL*tauH)*np.sin(phi)
    return np.sqrt(w0**2 - G*shift)/2/np.pi

def feedback_fwhm(phase, fwhm0, f0, G, c):
    w0 = 2*np.pi*f0
    dw0 = 2*np.pi*fwhm0
    phi = phase/180*np.pi
    shift = Omega_c*(1 - w0**2*tauL*tauH)*np.cos(phi) + w0**2*(tauL + tauH)*np.sin(phi)
    dw = dw0 - G*shift
    ix = dw < c
    dw[ix] = c
    return dw/2/np.pi

f0_model = lm.Model(feedback_f0)
f0_params = f0_model.make_params(f0=876, G=0, tauH=tauH)
f0_params['tauH'].set(vary=False)
# f0_params['phi0'].set(min=0)

fwhm_model = lm.Model(feedback_fwhm)
fwhm_params = fwhm_model.make_params(f0=876, fwhm0=0.1, G=0, c=0)
# fwhm_params['c'].set(vary=False)

T0 = 1 #K
D = '450nm'
pressure = 'dumped'

peak_params_files = glob('feedback_peak_parameters/{}_PP_{}_T{:.0f}_AM*.npy'.format(pressure, D, 1000*T0))
peak_params_files.sort(key=lambda fn: np.load(fn, allow_pickle=True).item()['AM'])

data0 = np.load('feedback_peak_parameters/{}_PP_{}_T{:.0f}_AM0.0.npy'.format(pressure, D, 1000*T0), allow_pickle=True).item()
fwhm0 = data0['fwhms'][:, 1].mean()
f0 = data0['f0s'][:, 1].mean()

fig_pp, ax_pp = plt.subplots(2, 1, sharex=True, figsize=(3.375, 2.4)) #plot the peak parameters themselves
ax_pp[0].set_ylabel('$f_0$ (Hz)')
ax_pp[1].set_ylabel('FWHM (Hz)')
ax_pp[-1].set_xlabel('detection phase $\phi$ (deg)')

fig_T, ax_T = plt.subplots(2, 1, sharex=True) #plot the calculated temperatures
ax_T[0].set_ylabel('He T (K)')
ax_T[1].set_ylabel('mode T (K)')
ax_T[1].set_xlabel('feedback phase shift (deg)')

AMs = []
f0Ts = []
max_AM = 10
# to_skip = [0.1, 0.5]
to_skip = []
Gs_f0 = []
Gs_fwhm = []
for file in peak_params_files:
    data = np.load(file, allow_pickle=True).item()
    if data['AM'] > max_AM or data['AM'] in to_skip:
        continue
    AM = data['AM']/100
    Vc = 10
    yrms = data['yrms'][:, 1]/5
    if len(data['f0s'][:,0]) < 2:
        continue
    print(data['AM'])
    f0mean = data['f0s'][:, 1].mean()
    f0_fit = f0_model.fit(data['f0s'][:, 1], f0_params, phase=data['f0s'][:, 0])
    G  = f0_fit.best_values['G']
    pl = ax_pp[0].errorbar(data['f0s'][:,0], data['f0s'][:,1], yerr=data['f0s'][:,2], fmt='o', 
                           label='G = {:.2e}'.format(G), ms=4)
    ax_pp[0].plot(data['f0s'][:, 0], f0_fit.best_fit, '--', color=pl[0].get_color())
    # ax_pp[0].axhline(0)
    print(f0_fit.best_values['tauH']*1e6)
    
    
    fwhm_params['G'].set(f0_fit.best_values['G'], vary=False)
    fwhm_params['f0'].set(f0_fit.best_values['f0'], vary=False)
    fwhm_fit = fwhm_model.fit(data['fwhms'][:, 1], fwhm_params, phase=data['fwhms'][:, 0])
    pl = ax_pp[1].errorbar(data['fwhms'][:,0], data['fwhms'][:,1], yerr=data['fwhms'][:,2], fmt='o', ms=4)
    ax_pp[1].plot(data['fwhms'][:, 0], fwhm_fit.best_fit, '--', color=pl[0].get_color())
    ax_pp[1].axhline(fwhm0)
    # ax_pp[1].set_yscale('log')
    
    if data['AM'] > -0.5:
        AMs.append(data['AM'])
        Gs_f0.append(f0_fit.best_values['G'])
        Gs_fwhm.append(fwhm_fit.best_values['G'])

    fwhmT = T0*fwhm0/data['fwhms'][:,1]
    f0T = calibration.f0_to_T(data['f0s'][:, 1], Vc=Vc, AM=AM, yrmsp=yrms)
    
    ax_T[0].plot(data['f0s'][:,0], f0T, 'o')
    ax_T[1].semilogy(data['fwhms'][:,0], fwhmT, 'o')

# ax_pp[0].legend(loc='best', ncol=2, frameon=False)
fig_T.tight_layout()
fig_pp.tight_layout()

# fig_pp.savefig('SVP_{:.0f}mK_{}.png'.format(1000*T0, D), dpi=600)

fig, ax = plt.subplots(1, 1)
ax.plot(AMs, Gs_f0, '-o', label='$f_0$ shift')
ax.plot(AMs, Gs_fwhm, '-s', label='FWHM shift')
ax.legend(loc='best')