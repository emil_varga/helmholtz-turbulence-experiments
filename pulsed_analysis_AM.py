# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: Davis
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from scipy.ndimage import gaussian_filter
from scipy.fft import fft

import os
from glob import glob
import sys

from tqdm import tqdm

plt.close('all')

# data_dir =  '../../data/RUN20210104/all/thermomechanics/Tsweep/2'
# data_dir = '../../data/RUN20210104/all/thermomechanics/ramps/10mKmin'
# data_dir = '../../data/RUN20210104/all/thermomechanics/ramps/2.5mKmin'
# data_dir = '../../data/RUN20210104/all/thermomechanics/ramps/fast'

rates = ['5mKmin', '10mKmin', '20mKmin', '50mKmin', '10000mKmin']
# drives = ['0.100', '0.200', '0.500', '1.000', '2.000', '5.000']
# drives = [8, 10, 12, 14, 16, 18, 20]
# drives = [20, 18, 16, 14, 12, 10, 8]
drives = [8, 9, 10, 11, 12]
# rates = ['50mKmin']
# drives = ['2.000']

# Ds = ['450', '750', '1800']
Ds = ['750']

# rates = ['0.001', '0.01', '0.02']
rates = ['']
# drives = ['']

out_dir = 'grids'
os.makedirs(out_dir, exist_ok=True)
plot = False

# ranges = {
#     '450': (10, 1000),
#     '750': (10, 1300)}

for D in Ds:
    for rate in rates:
        for drive in drives:
            # base_dir = 'Z:/emil/data/He-4/Helmholtz_turbulence/data/RUN20210104/dumped/{}nm/driven/AM/power_dependence/'.format(D)
            # # dataset = 'controlled/{}/AMD_20.000pc_{}V'.format(rate, drive)
            # dataset = '{}/AMD_20.000pc_{}V'.format(rate, drive)
            # output = "grid_AM_{}_{}_{}.npy".format(D, rate, drive)
            base_dir = 'Z:/emil/data/He-4/data/RUN20210104/dumped/{}nm/'.format(D)
            dataset = 'thermomechanics/lockin/Tsweep_fast/{:.0f}V'.format(drive)
            # dataset = 'thermomechanics/lockin/Tstepping'
            # dataset = 'thermomechanics/lockin/KZM/Tramp/repeated/{}Kmin'.format(rate)
            data_dir = os.path.join(base_dir, dataset)
            # output = 'grid_TM_450nm.npy'
            # output =  'grid_TM_450nm_{}Kmin.npy'.format(rate)
            output = 'grid_TM_{}nm_NM_{:.0f}Vc.npy'.format(D, drive)
            print(data_dir)
            
            files = glob(os.path.join(data_dir, '*.npy'))
            files.sort()
            print(len(files))
            if len(files) == 0:
                continue
            
            data0 = np.load(files[0], allow_pickle=True).item()            
            freqs = np.fft.fftfreq(data0['samples'], 1/data0['rate'])
            freqs = np.fft.fftshift(freqs)
            
            pulse_normalize=False
            pulse_ix = np.ones_like(freqs, dtype=bool)
            if 'pulse' in data0:
                pulse_ft = np.fft.fftshift(np.fft.fft(data0['pulse'], n=data0['samples']))
                pulse_ix = abs(pulse_ft) > 0.5*np.max(abs(pulse_ft))
                pulse_normalize=True
                        
            Tt = data0['samples']/data0['rate']
            # Ts = np.linspace(2.3, 0.45, 500)
            Ts = np.arange(0.5, 1.6, 0.025)
            grid = np.zeros((len(Ts), len(freqs[pulse_ix])), dtype='complex128')
            grid2 = np.zeros_like(grid, dtype=float)
            samples = np.zeros_like(grid, dtype=float)
            
            for file in tqdm(files):
                data = np.load(file, allow_pickle=True).item()
                Ti = data['Ti']
                Tf = data['Tf']
                T = 0.5*(Ti + Tf)
                # if Tf - Ti > 0.01: #only take the samples where the temperature is not changing too much
                #     continue

                z = data['timeseries'][0,:]# + 1j*data['timeseries'][1,:]
                resp = fft(z - z.mean(), workers=4)/np.sqrt(Tt)
                resp = np.fft.fftshift(resp)
                
                if pulse_normalize:                
                    resp /= pulse_ft
                
                if not np.isnan(T):
                    ix = np.argmin(np.abs(Ts - T))
                    # if abs(T - Ts[ix]) > 0.005:
                    #     continue
                    N = 1
                    # if Ts[ix] < 0.7:
                    #     print(T, N, Ts[ix])
                    grid[ix, :] += resp[pulse_ix]/N
                    grid2[ix, :] += abs(resp[pulse_ix])**2/N
                    samples[ix, :] += 1.0/N
            grid /= samples
            psd_grid = np.copy(grid2)/samples
            grid2 = np.sqrt(grid2/samples - abs(grid)**2)
            
            if plot:
                bg = np.nanmean(grid[Ts > 2.2, :], axis=0)
                # bg = 1
                
                fig, ax = plt.subplots(1, 1)
                ax.imshow(np.log(abs(grid/bg)), aspect='auto', extent=[freqs.min(), freqs.max(), Ts.min(), Ts.max()],
                          interpolation='bilinear', cmap='inferno', vmin=-1, vmax=5)
                ax.set_xlabel('frequency (Hz)')
                ax.set_ylabel('temperature (K)')
                
                fig, ax = plt.subplots(1, 1)
                ix = np.logical_and(Ts < 0.81, Ts > 0.79)
                x = np.nanmean(np.real(grid[ix, :]-bg), axis=0)
                y = np.nanmean(np.imag(grid[ix, :]-bg), axis=0)
                xs = gaussian_filter(x, 10)
                ys = gaussian_filter(y, 10)
                # bg = np.nanmean(grid[Ts > 2.2, :], axis=0)
                lx = ax.plot(freqs, x, 'o', ms=3)
                ly = ax.plot(freqs, y, 's', ms=3)
                
                ax.plot(freqs, xs, '-', ms=3, color=lx[0].get_color())
                ax.plot(freqs, ys, '-', ms=3, color=ly[0].get_color())
            
            samples_per_T = np.sum(samples, axis=1)
            Tmin = Ts[samples_per_T>0].min()
            Tix = Ts < 2
            Tix &= Ts > Tmin
            pfix = freqs[pulse_ix] > 0
            nfix = freqs[pulse_ix] < 0
            out = {'pgrid': grid[Tix][:,pfix], 'pfreqs': freqs[pulse_ix][pfix],
                   'pws': 1/grid2[Tix][:,pfix], #weights for the positive frequencies
                   'psd_pgrid': psd_grid[Tix][:,pfix],
                   'ngrid': grid[Tix][:,nfix], 'nfreqs': freqs[pulse_ix][nfix],
                   'nws': 1/grid2[Tix][:,nfix], #weights for the negative frequencies
                   'psd_ngrid': psd_grid[Tix][:,nfix],
                   'Ts': Ts[Tix],
                   'samples_per_T': samples_per_T[Tix]}
            np.save(os.path.join(out_dir, output), out)
