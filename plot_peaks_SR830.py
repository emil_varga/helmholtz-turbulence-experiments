# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 14:16:05 2019

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import os.path as path
from glob import glob

plt.close('all')
files = []

# data_dir = '../../data/RUN20210104/1800nm/T900/fsweeps/30dB/1'
# files += glob(path.join(data_dir, '*.npy'))
# data_dir = '../../data/RUN20210104/1800nm/T800/fsweeps/30dB/1'
# files += glob(path.join(data_dir, '*.npy'))
data_dir = '../../data/RUN20210104/1800nm/T700/fsweeps/30dB/0'
files += glob(path.join(data_dir, '*.npy'))

# data_dir = '../../data/RUN20210104/1800nm/T900/fsweeps/thermo'
files += glob(path.join(data_dir, '*.npy'))
files.sort()

print(len(files))


fig, ax = plt.subplots(1, 1)
As = []
Rs = []
cmap = plt.get_cmap('jet')
Vmax = 1
for file in files[:]:
    d = np.load(file, allow_pickle=True).item()
    if 'probe_amplitude_Vrms' in d:
        Ap = d['probe_amplitude_Vrms']
        A = d['pump_amplitude_Vpp']
        # A = d['pump_frequency']
        # if d['probe_amplitude_Vrms'] != 1:
        #     continue
        # if not abs(d['pump_amplitude_Vpp'] - 20) < 1e-3:
        #     continue
        # if d['pump_frequency'] not in [315]:
        #     continue
    elif 'drive_amplitude_Vpp' in d:
        A = d['drive_amplitude_Vpp']
    else:
        print('Unknown amplitude.')
        A = 1

    data = d['data']
    # t = data[:,3].astype(float)
    f = data[:,0]
#    if f[0] < f[-1]:
#        continue
    x = data[:,1].astype(float)
    y = data[:,2].astype(float)
    if A == 0:
        A = 1
    r = np.sqrt(x**2 + y**2)/A
    # r /= r.min()
    print(A, f[np.argmax(r)])
    ax.plot(f, r, '-o', lw=0.5, ms=1, color=cmap(np.sqrt(A/Vmax)))
    As.append(A)
    Rs.append(y[f>60].mean())

fig, ax = plt.subplots(1,1)
ax.plot(As, Rs, 'o')