# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 11:52:39 2021

@author: Davis
"""


import numpy as np
import matplotlib.pyplot as plt
import os
import time

import sys
sys.path.append("D:\\ev")
from instruments.DAQcard import DAQcard

import ltwsclient as ltwsc
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

data_dir = '../../data/RUN20210104/dumped/750nm/driven/DAQ_battery/power_dependence/0'


DAQrate = 8192
pulse_t = 32 #s, total duration of the pulse
pulse_ti = 0.5 #s, start of the chrip
pulse_fi = 100 #Hz
pulse_tf = 31.5 #s, end of the chirp
pulse_ff = 1300 #Hz
pulse_tw = 1 #Rise/fall width
# pulse_amplitudes=np.linspace(0.522, 5, 10)
pulse_amplitudes = [3, 5]
# pulse_amplitudes = np.linspace(1, 0, 8)

time_per_power = 2*60*60 #2 hours
max_T = 2.5 #don't measure above this temperature

DAQsamples = int(DAQrate*32)

def envelope_function(t, ti, tf, tw):
    tt = np.atleast_1d(t)
    left = np.exp(-(tw/(tt - ti))**2)
    right = np.exp(-(tw/(tt - tf))**2)
    env = left*right
    ix0 = np.logical_or(tt < ti, tt > tf)
    env[ix0] = 0
    return env

def chirped_pulse(t, ti, tf, tw, fmin, fmax):
    dt = tf - ti
    df = fmax - fmin
    phi = fmin*(t - ti) + 0.5*df*(t - ti)**2/dt
    chirp = np.sin(2*np.pi*phi)
    env = envelope_function(t, ti, tf, tw)
    return chirp*env


plt.close('all')

pulse_ts = np.linspace(0, pulse_t, pulse_t*DAQrate)
pulse = chirped_pulse(pulse_ts, pulse_ti, pulse_tf, pulse_tw, pulse_fi, pulse_ff)

pulse_ft = np.fft.rfft(pulse, n=DAQsamples)
fres = np.fft.rfftfreq(DAQsamples, 1/DAQrate)


try:
    while True:
        for pulse_amplitude in pulse_amplitudes:
            daq = DAQcard(['ai0'], rate=DAQrate, samples=DAQsamples, min_val=-10, max_val=10,
                          outputs=('ao0', pulse*pulse_amplitude))
            output_dir = os.path.join(data_dir, 'AMP_{:.0f}mV'.format(1000*pulse_amplitude))
            os.makedirs(output_dir, exist_ok=True)
            start_time = time.time()
            while time.time() - start_time < time_per_power:
                Ti = lt.get_T3()
                if Ti > max_T:
                    time.sleep(1)
                    continue
                timestamp = time.strftime('%Y%m%d-%H%M%S')
                data = daq.write_measure()
                Tf = lt.get_T3()
                print(timestamp)
                output = {'timeseries': data, 'Ti': Ti, 'Tf': Tf, 'pulse': pulse,
                          'pulse_amplitude': pulse_amplitude,
                          'samples': DAQsamples, 'rate': DAQrate}
                np.save(os.path.join(output_dir, 'DM_{}.npy'.format(timestamp)), output)
            daq.close()
finally:
    daq.close()        