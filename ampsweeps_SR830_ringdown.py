# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: Davis
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime
from tqdm import tqdm
import nidaqmx
import multiprocessing as mp

def task_read(queue, DEV, RATE, N):
    intask = nidaqmx.Task()
    #signal
    intask.ai_channels.add_ai_voltage_chan("/%s/%s" % (DEV, "ai0"), 
                                           min_val=-2, max_val=2)
    #reference
    intask.ai_channels.add_ai_voltage_chan("/%s/%s" % (DEV, "ai1"), 
                                           min_val=-10, max_val=10)
    
    intask.timing.cfg_samp_clk_timing(rate=RATE, samps_per_chan=N, 
                                      sample_mode=nidaqmx.constants.AcquisitionType.FINITE)
    indata = np.empty((N,2))
    i = 0
    print("Starting to read!")
    intask.start() #start reading daq card
    while i < N:
        d = intask.read(1000)
        indata[i:i + len(d[0]),0] = np.array(d[0])
        indata[i:i + len(d[0]),1] = np.array(d[1])
        i += len(d[0])
        print(i)
    intask.stop()
    intask.close()
    queue.put(indata)

if __name__ == '__main__':
    DEV = 'Dev2'
    RATE = 20000
    N = 3*RATE #10 seconds
    
    output_dir = '../data/RUN6/ringdown_test'
    
    amp_ranges = [(0, 1, 300)] #up, down, jump-down
    
    
    freqs = [930]
    
    reps = 1
    
    os.makedirs(output_dir,exist_ok=True)
            
    rm = visa.ResourceManager()
    lockin = rm.open_resource('GPIB1::8::INSTR')
    lockin.write('FMOD 1') #internal reference
    lockin.write('ISRC 0')
    lockin.write('IGNT 0')
    lockin.write('SENS 24') # 200mV sensitivity
    lockin.write('OFLT 7') # 30 ms time constant
    
    for r in range(reps):
        for freq in freqs:
            lockin.write('FREQ {}'.format(freq)) #set the frequency
            for ar in amp_ranges:
                amps = np.linspace(ar[0], ar[1], ar[2])
                timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                xs, ys = [], []
                print("Starting {} V, {} Hz".format(ar, freq))
                lockin.write('SLVL {:.3f}'.format(amps[0])) #set the first amplitude
                time.sleep(5)
                for amp in tqdm(amps):
                    lockin.write('SLVL {}'.format(amp))
                    time.sleep(0.1)
                    resp = lockin.query('SNAP? 1,2')
                    xstr, ystr = resp.split(',')
                    x = float(xstr)
                    y = float(ystr)
                    xs.append(x)
                    ys.append(y)
                
                Q = mp.Queue()
                p = mp.Process(target = task_read, args=(Q,DEV,RATE,N,))
                p.start()
                time.sleep(2)
                lockin.write('SLVL 0.05') #turn off output
                print('output off')
                indata = Q.get()
                p.join()            
                            
                data = np.column_stack((amps, xs, ys))
                print(data.shape)
                out = {'frequency' : freq, 'data' : data}
                np.save(path.join(output_dir, 'AS_SR_'+timestamp), out, allow_pickle=True)
                np.save(path.join(output_dir, 'RING_'+timestamp), indata, allow_pickle=True)
                
    lockin.close()