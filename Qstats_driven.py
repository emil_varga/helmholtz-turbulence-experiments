# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: emil
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
from fano_peak import DoubleFanoPeak
from fano_peak import SingleFanoPeak

import os
from glob import glob

plt.close('all')

# rates = ['0.002', '0.05', '0.1', '0.5', '1']
# rates = ['0.05', '0.02', '0.01', '0.005', '0.002']
peak_class = DoubleFanoPeak
drive = 100
rates = ['0.01', '0.02', '0.05']
rates = [0]
Tt = 0.7
Ttol = 0.002

#thermomechanics, 800 mK
# f1 = 1205
# f2 = 1215

#thermomechanics, 600 mK
# f1 = 1210
# f2 = 1214

#drive, 800 mK
f1 = 1180
f2 = 1240

frates = []
allQ1s = []
allQ1es = []
allQ2s = []
allQ2es = []

samples_per_fit = 5
skip_files=0

for r in rates:
    Trate = '{}Kmin'.format(r)
    print(Trate)
    # base_dir = r"D:\ev\data\RUN20210104\dumped\750nm\driven\DAQ_battery\KZM\Tramp\repeated"
    # run_dir = "{}/AMP_{:.0f}mV".format(Trate, drive)
    base_dir = r"D:\ev\data\RUN20210104\dumped\750nm\driven\DAQ_battery\power_dependence"
    run_dir = "0/AMP_{:.0f}mV".format(drive)
    data_dir = os.path.join(base_dir, run_dir)
    
    files = glob(os.path.join(data_dir, '*.npy'))
    files.sort()
    print(len(files))
    
    nscale=1
    Q1s = []
    Q2s = []
    samples = 0
    avg_resp = 0
    to_skip = skip_files
    for file in files:
        data = np.load(file, allow_pickle=True).item()
        Ti = data['Ti']
        Tf = data['Tf']
        if (max(Ti, Tf) > Tt + Ttol) or (min(Ti, Tf) < Tt - Ttol):
            to_skip = skip_files
            continue
        to_skip -= 1
        if to_skip > 0:
            continue
        z = data['timeseries']
        nperseg = data['samples']
        noverlap = None#int(nperseg/2)
        nfft = None#data['samples']
        # freqs, resp = sig.welch(z, fs=data['rate'],
        #                         nperseg=nperseg, noverlap=noverlap, nfft=nfft, return_onesided=True)
        # freqs, resp = sig.periodogram(z, fs=data['rate'])
        fft = np.fft.rfft(z)
        pulse_fft = np.fft.rfft(data['pulse'])
        freqs = np.fft.rfftfreq(data['samples'], 1/data['rate'])
        # resp = np.real(fft)
        resp = np.abs(fft/pulse_fft*abs(pulse_fft))**2
        # resp = np.sqrt(resp)
        samples += 1
        avg_resp += resp
        if samples >= samples_per_fit:
            avg_resp /= samples
            try:
                ixp = np.logical_and(freqs > min(f1, f2), freqs < max(f1, f2))
                peakp = peak_class(freqs[ixp], avg_resp[ixp], plot=True)
                peakp.ax.set_title(r)
                peakp.fig.tight_layout()
                Q1s.append(peakp.Q1)
                Q2s.append(peakp.Q2)
            except RuntimeError:
                print("Plot failed.")
            samples = 0
            avg_resp = 0
    if samples > 0:
        avg_resp /= samples
        try:
            ixp = np.logical_and(freqs > min(f1, f2), freqs < max(f1, f2))
            peakp = peak_class(freqs[ixp], avg_resp[ixp], plot=True)
            peakp.ax.set_title(r)
            peakp.fig.tight_layout()
            Q1s.append(peakp.Q1)
            Q2s.append(peakp.Q2)
        except RuntimeError:
            print("Plot failed.")
    frates.append(float(r))
    allQ1s.append(np.mean(Q1s))
    allQ1es.append(np.std(Q1s))
    allQ2s.append(np.mean(Q2s))
    allQ2es.append(np.std(Q2s))

fig, ax = plt.subplots(1, 1)
ax.errorbar(frates, allQ1s, yerr=allQ1es, fmt='-o')
ax.errorbar(frates, allQ2s, yerr=allQ2es, fmt='-s')
ax.set_xscale('log')

