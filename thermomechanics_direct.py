# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 16:15:05 2020

@author: Davis
"""


import numpy as np
import visa
import matplotlib.pyplot as plt
import time
import os
import ltwsclient

import sys
sys.path.append('D:\\ev')
from instruments.DAQcard import DAQcard


print('Connecting to logger.')
server = 'onnes.ccis.ualberta.ca'
port = 3172
lt = ltwsclient.LTWebsockClient(server, port)
print('Connected')

data_dir = '../../data/RUN20210104/all/thermomechanics/ramps/2.5mKmin'

#DAQ settings
daq_channels = ['ai0']
rate = 8192 #Hz
samples = int(rate*32) #8 s

os.makedirs(data_dir, exist_ok=True)

try:
    #setup DAQ
    daq = DAQcard(daq_channels, rate, samples, min_val=-2, max_val=2)
    while True:
        timestamp = time.strftime('%Y%m%d-%H%M%S')
        try:
            Ti = lt.get_T3()
        except:
            Ti = np.nan
        print(timestamp, end='')
        timeseries = daq.measure()
        print(' ...done')
        try:
            Tf = lt.get_T3()
        except:
            Tf = np.nan
        output = {'timeseries': timeseries, 'rate': rate, 'samples': samples,
                  'Ti': Ti, 'Tf': Tf}
        filename = os.path.join(data_dir, 'TM_{}.npy'.format(timestamp))
        np.save(filename, output, allow_pickle=True)
finally:
    daq.close()