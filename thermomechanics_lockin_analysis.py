# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: emil
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from scipy.ndimage import gaussian_filter
import scipy

import os
from glob import glob

import matplotlib as mpl

files = []

data_dir = r'Z:/emil/data/He-4/Helmholtz_turbulence/data/RUN20210104/dumped/450nm/thermomechanics/lockin/KZM/Tramp/repeated/0.02Kmin'
files = glob(os.path.join(data_dir, '*.npy'))
data_dir = r'Z:/emil/data/He-4/Helmholtz_turbulence/data/RUN20210104/dumped/450nm/thermomechanics/lockin/KZM/Tramp/repeated/0.01Kmin'
files += glob(os.path.join(data_dir, '*.npy'))
data_dir = r'Z:/emil/data/He-4/Helmholtz_turbulence/data/RUN20210104/dumped/450nm/thermomechanics/lockin/KZM/Tramp/repeated/0.001Kmin'
files += glob(os.path.join(data_dir, '*.npy'))
files.sort()
print(len(files))

nscale=1

data0 = np.load(files[0], allow_pickle=True).item()
freqs = np.fft.fftshift(np.fft.fftfreq(nscale*data0['samples'], 1/data0['rate']))
Tt = nscale*data0['samples']/data0['rate']
N = data0['samples']

Ts = np.linspace(2.3, 0.45, 500)
grid = np.zeros((len(Ts), len(freqs)))
samples = np.zeros_like(grid)

for file in files:
    data = np.load(file, allow_pickle=True).item()
    z = data['timeseries'][0,:] + 1j*data['timeseries'][1,:]
    z -= z.mean()
    resp = np.abs(np.fft.fftshift(scipy.fft.fft(z, workers=4, n=nscale*N)))/np.sqrt(Tt)
    if 'pulse' in data:
        print('Normalizing')
        pulse_ft = np.fft.fftshift(np.fft.fft(data['pulse'], n=nscale*N))
        resp /= pulse_ft/abs(pulse_ft)
    Ti = data['Ti']
    Tf = data['Tf']
    T = 0.5*(Ti + Tf)
    if abs(Ti - Tf) > 0.05:
        continue
    if not np.isnan(T):
        # ix = np.argmin(np.abs(Ts - T))
        ix = np.logical_and(Ts > min(Ti, Tf), Ts < max(Ti, Tf))
        Nix = sum(ix)
        if Nix == 0:
            # print("{} ({}-{}) Doesn't belong anywhere!".format(T, Ti, Tf))
            ix = np.argmin(np.abs(Ts - T))
            Nix = 1
        print(T, Nix, max(np.abs(data['timeseries'][0,:] + 1j*data['timeseries'][1,:])))
        grid[ix, :] += resp/Nix
        samples[ix, :] += 1.0/Nix
grid /= samples
ix = Ts > 1.6
ix &= Ts < 1.8
bg = np.nanmean(grid[ix, :], axis=0)
# grid[np.isnan(grid)] = 0
# bg=1

np.savetxt('background_450.txt', np.c_[freqs, bg])

plt.close('all')
fig, ax = plt.subplots(1, 1)#, figsize=(4, 5.7))
ax.imshow(np.log(grid/bg), aspect='auto', extent=[freqs.min(), freqs.max(), Ts.min(), Ts.max()],
          interpolation='nearest', vmin=-1)
ax.set_xlabel('frequency (Hz)')
ax.set_ylabel('temperature (K)')

fig, ax = plt.subplots(1, 1)
ix = np.logical_and(Ts < 2, Ts > 0.59)
y = np.nanmean(grid[ix, :], axis=0)
N = len(freqs)
N2 = int(N/2)
ax.plot(freqs[N2:], y[N2:], '-o', ms=3)
ax.plot(freqs[N2:], y[1:N2+1][::-1], '-o', ms=3)
# np.savetxt("KZM0.005pps_600mK.txt", np.c_[freqs, y])
