#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 17:01:02 2019

@author: emil
"""

import numpy as np
from ziLockin import ziLockin
from datetime import datetime
import os.path as path
import os
import msvcrt
import time

import ltwsclient as ltwsc

def write_ampsweep_info(filename, timestamp, freq, harm, sample_rate):
    infos = [
        "#timestamp = {}\n".format(timestamp),
        "#frequency = {}\n".format(freq),
        "#harmonic  = {}\n".format(harm),
        "#sample rate = {}\n".format(sample_rate)]
    with open(filename, 'w') as file:
        for line in infos:
            file.write(line)

device_id = 'dev538'
continuous = True

RUN = 'RUN_20200712'

output_dir = '../data/{}/lockin_ampsweeps/UM/T1000/'.format(RUN)

T3max = 1.025
sampless = [50, 100, 200]
reps = 2

fs = [(1633.7, 0.0, 0.15)]

#freq_i = 1240
#freq_f = 1280
#freq_steps = 200
#
#freqs = np.linspace(freq_i, freq_f, freq_steps)
#Ai = 0
#Af = 10e-3

#fs = [(f, Ai, Af) for f in freqs]

input_channel  = 0
output_channel = 6
output_id      = 0
osc            = 0
demod_id       = 0
sample_rate    = 300
time_constant  = 200e-3
#harmonic       = 1

#data readout from logger
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

os.makedirs(output_dir,exist_ok=True)

def get_T3(lt):
    attempts = 0
    while True:
        try:
            last_5min = lt.get_last_measurements('3hepot_t', 5*60)
            if len(last_5min) == 0:
                continue
            attempts = 0
        except:
            attempts+=1
            if attempts > 5:
                print('cant get data from logger')
                raise
            continue
        if attempts > 5:
            raise RuntimeError("Something's wrong.")
        #check that the data are up-to-date
        if abs(time.time() - last_5min[-1][0]) > 600:
            raise RuntimeError('old data from logger')
        #there is some weird offset of about 80 s between the logger time and local time
        t = last_5min[:, 0]
        t0 = t[-1]
        T3 = last_5min[t > t0-30,1].mean() #average only over the last 30 s
        break
    return T3
        

def wait_for_T3(lt, cutoff_T3, sleep_time=30):
    print(f"Waiting for T3 to go below {T3max}.")
    while True:
        T3 = get_T3(lt)
        print(f"T3 is {T3}")
        if T3 < cutoff_T3:
            break
        time.sleep(sleep_time)
    return T3

if __name__ == '__main__':
    print('Connecting to lockin.')
    lockin = ziLockin(device_id)
    
    lockin.configure_input(input_channel, input_range = 1, 
                           ac_coupling=True, imp50 = False,
                           differential=False)
    demods = [0,1,2]#,3,4,5]
    harms = [1,2,3]#,4,6,8]
    
    for d, h in zip(demods,harms):
        lockin.configure_demodulator(d, sample_rate, input_channel = input_channel, filter_order=4, 
                                     tc = time_constant, osc=osc, harm=h)
    not_done = True
    while not_done:
        for rep in range(reps):
            for samples in sampless:
                k = 0
#                for f, Ai, Af in fs:
                while k < len(fs):
                    T3start = wait_for_T3(lt, T3max)
                    f, Ai, Af = fs[k]
                    
                    print("Starting ", f, Ai, Af)
                    now = datetime.now()
                    timestamp = now.strftime('%Y%m%d-%H_%M_%S')
                    time.sleep(2)
                    sweep_data = lockin.amp_sweep(Ai, Af, samples, f, osc, output_id, output_channel, demods = demods,
                                                  timeout = np.inf,verbose=True,
                                                  ramp_down=True)
                    T3end = get_T3(lt)
                    sweep_data['_Tstart'] = T3start
                    sweep_data['_Tend'] = T3end
                    if T3end < T3max:
                        k += 1
                
                    np.save(path.join(output_dir, 'AS-'+timestamp), sweep_data)
        
        for rep in range(reps):
            for samples in sampless:
                k=0
#                for f, Ai, Af in fs:
                while k < len(fs):
                    T3start = wait_for_T3(lt, T3max)
                    f, Ai, Af = fs[k]
                    print("Starting (jump-up-ramp-down)", f, Ai, Af)
                    now = datetime.now()
                    
                    sweep_data = lockin.amp_sweep(Ai, Af, samples, f, osc, output_id, output_channel, demods = demods,
                                                  timeout = np.inf,verbose=True,
                                                  ramp_up=False)
                    T3end = get_T3(lt)
                    sweep_data["_Tstart"] = T3start
                    sweep_data["_Tend"] = T3end
                    if T3end < T3max:
                        k += 1
                    
                    timestamp = now.strftime('%Y%m%d-%H_%M_%S')
                    np.save(path.join(output_dir, 'AS-'+timestamp), sweep_data)
        if not continuous:
            not_done = False
        else:
            if msvcrt.kbhit():
                c = msvcrt.getch()
                if c == 'q':
                    not_done = False
    
    lockin.disable_everything()
