# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: emil
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
from psd_peak import SingleFanoPeak
from scipy.ndimage import gaussian_filter

import os
from glob import glob

plt.close('all')

smooth=0

# input_files = glob('grids/grid_AM_1800_50mKmin_5.000.npy')
input_files = glob('grids/grid_TM_750nm_NM_12Vc.npy')
# input_files += glob('grids/grid_AM_450_50mKmin*.npy')
print(input_files)

plot=False
for input_file in input_files:
    print("Starting on ", input_file)
    data = np.load(input_file, allow_pickle=True).item()
    spgrid = data['psd_pgrid']
    sngrid = data['psd_ngrid']
    pfreqs = data['pfreqs']
    nfreqs = data['nfreqs']
    Tgs = data['Ts']
        
    fig, ax = plt.subplots(1, 2)
    fig.suptitle(input_file)
    ax[0].imshow(np.log(abs(sngrid)), aspect='auto', cmap='inferno',
                 extent=(nfreqs.min(), nfreqs.max(), Tgs.min(), Tgs.max()), origin='lower')
    ax[1].imshow(np.log(abs(spgrid)), aspect='auto', cmap='inferno',
                 extent=(pfreqs.min(), pfreqs.max(), Tgs.min(), Tgs.max()), origin='lower')
    
    # Ts = np.linspace(Tgs.min(), Tgs.max), 50)
    Ts = Tgs
    dT = np.mean(np.diff(Ts))
    
    f0s = []
    fwhms = []
    f0es = []
    fwhmes = []
    pp0 = None
    np0 = None

    actual_Ts = []
    for T in Ts:
        Tix = abs(T - Tgs) < dT/2
        print(sum(Tix))
        w = data['samples_per_T'][Tix]
        W = np.nansum(w)
        if np.isnan(W) or W==0:
            continue
        nresp = np.nansum(sngrid[Tix, :]*w[:, np.newaxis], axis=0)/W
        presp = np.nansum(spgrid[Tix, :]*w[:, np.newaxis], axis=0)/W
        ixp = pfreqs < 1250
        ixp &= pfreqs > 1180
        ixn = abs(nfreqs) < 1250
        ixn &= abs(nfreqs) > 1180
        try:
            ppeak = SingleFanoPeak(pfreqs[ixp], presp[ixp], p0=pp0, plot=plot, npeaks=1)
            if 'ax' in ppeak.__dict__:
                ppeak.ax.set_title('T = {} K'.format(T))
                ppeak.ax.set_yscale('log')
            pp0 = ppeak.out.best_values
            npeak = SingleFanoPeak(nfreqs[ixn], nresp[ixn], p0=np0, plot=plot, npeaks=1)
            if 'ax' in npeak.__dict__:
                npeak.ax.set_title('T = {} K'.format(T))
                npeak.ax.set_yscale('log')
            np0 = npeak.out.best_values
            f0_fits = [abs(peak.f0) for peak in [ppeak, npeak]]
            fwhm_fits = [abs(peak.fwhm) for peak in [ppeak, npeak]]
            f0s.append(np.mean(f0_fits))
            f0es.append(np.std(f0_fits))
            fwhms.append(np.mean(fwhm_fits))
            fwhmes.append(np.std(fwhm_fits))
            actual_Ts.append(T)
        except Exception as e:
            raise
            print(e)
            fwhms.append(np.nan)
            f0s.append(np.nan)
            fwhmes.append(np.nan)
            f0es.append(np.nan)
            print("Fit failed.")
    
    fig, ax = plt.subplots(1, 1)
    ax.plot(actual_Ts, fwhms,'o')
    ax.set_title('fwhm')
    
    fig, ax = plt.subplots(1, 1)
    ax.plot(actual_Ts, f0s,'o')
    ax.set_title('f0')
    out_file = input_file.replace('grid_','').replace('npy', 'txt')
    print(out_file)
    np.savetxt(out_file, np.c_[actual_Ts, fwhms, f0s, fwhmes, f0es])

