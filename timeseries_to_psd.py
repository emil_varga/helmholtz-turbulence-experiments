# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 14:45:45 2021

@author: ev
"""

import numpy as np
import scipy.signal as sig

import os
from os import path
from glob import glob
from tqdm import tqdm

from multiprocessing import Pool

# D = '450'

base_dir = r'Z:\emil\data\He-4\data\RUN20210104\SVP'
output_base = 'C:/Users/ev/physics/data/RUN20210104/SVP/PSDs'

feedback_C = 5.403e-9 #F
feedback_Rin = 249+5000 #Ohm
baserate = 8192*2
base_samplet = 8 #s
nperseg = int(base_samplet*baserate)
noverlap = int(0.5*nperseg)

def feedback_RC(f, R=feedback_Rin, C=feedback_C):
    w = 2*np.pi*f
    nom = 1j*w*R*C + (w*R*C)**2
    denom = 1 + (w*R*C)**2
    return nom/denom

def process_deg_dir(DEGdir, Dstr, Tstr, AMstr):
    DEGstr = path.split(DEGdir)[-1][4:-4]
    print(Dstr, Tstr, AMstr, DEGstr)
    output_dir = path.join(output_base, Dstr, 'T'+Tstr, 'AM'+AMstr)
    output_file = path.join(output_dir, 'AM{}_{}DEG.npy'.format(AMstr, DEGstr))
    os.makedirs(output_dir, exist_ok=True)
    files = glob(path.join(DEGdir, 'FB*.npy'))
    phase = np.deg2rad(float(DEGstr))
    avg_respx = 0
    avg_respy = 0
    yrms = 0
    avg_T = 0
    for file in files:
        data = np.load(file, allow_pickle=True).item()
        z = data['timeseries'][0, :] + 1j*data['timeseries'][1, :]
        rate = data['rate']
        samples = data['samples']
        if data['rate'] != baserate: #we need to resample the data
            # print("Resampling!")
            new_samples = int(baserate/rate*samples)
            zr = sig.resample(z, num=new_samples)
            z = zr
        yfs, Pyy = sig.welch(np.imag(z), fs=baserate, scaling='density',
                             return_onesided=True, nperseg=nperseg, noverlap=noverlap)
        yy = np.trapz(Pyy*abs(feedback_RC(yfs)), yfs)
        yrms += np.sqrt(yy)/len(files)
        avg_T += 0.5*(data['Ti'] + data['Tf'])/len(files)
        z = z *np.exp(1j*phase)
        freqs, Pxx = sig.welch(np.real(z), fs=baserate, return_onesided=True, scaling='density',
                               nperseg=nperseg, noverlap=noverlap)
        _, Pyy = sig.welch(np.imag(z), fs=baserate, return_onesided=True, scaling='density',
                               nperseg=nperseg, noverlap=noverlap)        
        avg_respx += Pxx/len(files)
        avg_respy += Pyy/len(files)
    np.save(output_file, {'psdx': np.c_[freqs, avg_respx], 'psdy': np.c_[freqs, avg_respy], 'yrms_V': yrms, 
                          'T': avg_T, 'D': float(Dstr[:-2]), 'phase': float(DEGstr), 'AMd': float(AMstr),
                          'carrier_A': data['carrier_A'],
                          'carrier_f': data['carrier_f'],
                          'sensitivity': data['lia_sens'],
                          'preamp_gain': data['preamp_gain'],})

if __name__ == '__main__':
    Ddirs = glob(path.join(base_dir, '*nm'))
    for Ddir in Ddirs:
        Dstr = path.split(Ddir)[-1]
        Tdirs = glob(path.join(base_dir, '{}/thermomechanics/lockin/feedback/T*'.format(Dstr)))
        for Tdir in Tdirs:
            Tstr = path.split(Tdir)[-1][1:]
            AMdirs = glob(path.join(Tdir, 'SENS50mV/AM*'))
            for AMdir in AMdirs:
                AMstr = path.split(AMdir)[-1][2:]
                DEGdirs = glob(path.join(AMdir, 'PHI_*_DEG'))
                p = Pool(6)
                for DEGdir in DEGdirs:
                    p.apply_async(process_deg_dir, args=(DEGdir, Dstr, Tstr, AMstr))
                p.close()
                p.join()
