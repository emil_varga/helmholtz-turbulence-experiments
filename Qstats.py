# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: emil
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
from fano_peak import SingleFanoPeak

import os
from glob import glob

plt.close('all')

# rates = ['0.002', '0.05', '0.1', '0.5', '1']
# rates = ['0.05', '0.02', '0.01', '0.005', '0.002']
rates = ['0.001', '0.01', '0.02']
Tt = 0.8
Ttol = 0.002

#thermomechanics, 800 mK
# f1 = 1205
# f2 = 1215

#thermomechanics, 600 mK
# f1 = 1210
# f2 = 1214

#drive, 800 mK
f1 = 872
f2 = 866

frates = []
allQs = []
allQes = []

samples_per_fit = 5
skip_files=0

for r in rates:
    Trate = '{}Kmin'.format(r)
    print(Trate)
    data_dir = r'D:/ev/data/RUN20210104/dumped/450nm/thermomechanics/lockin/KZM/Tramp/repeated/{}'.format(Trate)
    
    files = glob(os.path.join(data_dir, '*.npy'))
    files.sort()
    print(len(files))
    
    nscale=1
    Qs = []
    samples = 0
    avg_resp = 0
    to_skip = skip_files
    for file in files:
        data = np.load(file, allow_pickle=True).item()
        Ti = data['Ti']
        Tf = data['Tf']
        if (max(Ti, Tf) > Tt + Ttol) or (min(Ti, Tf) < Tt - Ttol):
            to_skip = skip_files
            continue
        to_skip -= 1
        if to_skip > 0:
            continue
        z = data['timeseries'][0,:] + 1j*data['timeseries'][1,:]
        nperseg = data['samples']/2
        noverlap = None#int(nperseg/2)
        nfft = None#data['samples']
        freqs, resp = sig.welch(z, fs=data['rate'],
                            nperseg=nperseg, noverlap=noverlap, nfft=nfft, return_onesided=False)
        # resp = np.sqrt(resp)
        samples += 1
        avg_resp += resp
        if samples >= samples_per_fit:
            avg_resp /= samples
            try:
                ixp = np.logical_and(freqs > min(f1, f2), freqs < max(f1, f2))
                ixn = np.logical_and(freqs > min(-f1, -f2), freqs < max(-f1, -f2))
                peakp = SingleFanoPeak(freqs[ixp], avg_resp[ixp])
                peakn = SingleFanoPeak(freqs[ixn], avg_resp[ixn], plot=True)
                peakn.ax.set_title(r)
                peakn.fig.tight_layout()
                Qs.append(0.5*(abs(peakp.Q) + abs(peakn.Q)))
            except RuntimeError:
                print("Plot failed.")
            samples = 0
            avg_resp = 0
    if samples > 0:
        avg_resp /= samples
        try:
            ixp = np.logical_and(freqs > min(f1, f2), freqs < max(f1, f2))
            ixn = np.logical_and(freqs > min(-f1, -f2), freqs < max(-f1, -f2))
            peakp = SingleFanoPeak(freqs[ixp], avg_resp[ixp])
            peakn = SingleFanoPeak(freqs[ixn], avg_resp[ixn], plot=True)
            peakn.ax.set_title(r)
            peakn.fig.tight_layout()
            Qs.append(0.5*(abs(peakp.Q) + abs(peakn.Q)))
        except RuntimeError:
            print("Plot failed.")
    frates.append(float(r))
    allQs.append(np.median(Qs))
    allQes.append(np.std(Qs))

fig, ax = plt.subplots(1, 1)
ax.errorbar(frates, allQs, yerr=allQes, fmt='-o')
ax.set_xscale('log')

