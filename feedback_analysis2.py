# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 12:14:52 2021

@author: ev
"""

import numpy as np
import os.path as path
import matplotlib.pyplot as plt
import scipy.fft as fft
import scipy.interpolate as intp
import scipy.signal as sig

from tqdm import tqdm

from scipy.ndimage import gaussian_filter

plt.style.use('aps')

# from psd_peak import DoubleFanoPeak
import psd_peak
peak_class = psd_peak.SingleFanoPeak

from glob import glob

rng = np.random.default_rng()

def get_phase(file):
    return np.load(file, allow_pickle=True).item()['phase']

def get_AMdepth(AMdir):
    return float(path.split(AMdir)[-1][2:])

def good_ix(x):
    xs = gaussian_filter(x, 5)
    dx = np.std(x-xs)
    return abs(x - xs) < 5*dx

plt.close('all')

cmap = plt.get_cmap('viridis')

T = 1
D = '450nm'

pressure='dumped'

base_dir = 'C:/Users/ev/physics/data/RUN20210104/{}/PSDs/{}/T{:.0f}/'.format(pressure, D, 1000*T)
print(base_dir)
AM_dirs = glob(path.join(base_dir, 'AM*'))
AM_dirs.sort(key=get_AMdepth, reverse=False)
AMdepths = [get_AMdepth(d) for d in AM_dirs]
print(AMdepths)
AMdepths = [10]

#for 1.3 K
# peak_f1 = 780
# peak_f2 = 880

#for 0.7 K
peak_f1 = 850
peak_f2 = 890
TM_peak_f1 = 860
TM_peak_f2 = 870

# peak_f1 = 500
# peak_f2 = 1500

bf1 = 920
bf2 = 940

#for 1 K
peak_f1 = 880
peak_f2 = 830

#for 750 nm, 1 K and 0.7 K
# peak_f1 = 1180
# peak_f2 = 1220

# peak_f1 = 858
# peak_f2 = 862

noise_peaks = [
    (857, 857.6),
    (910, 912)]

bootstrap_B = 100
plot=True
fig_X, ax_X = plt.subplots(1, 1, figsize=(3.375, 3.375))
fig_Y, ax_Y = plt.subplots(1, 1, figsize=(3.375, 3.375))
ax_all = [ax_X, ax_Y]
p0 = None
f01 = []
f02 = []
for AM in AMdepths: #loop through all AM depths
    integral = []
    integrale = []
    fwhms = []
    f0s = []
    fwhmse = []
    f0se = []
    yrmss = []
    p0p = None
    p0n = None
    f0 = None
    files = glob(path.join(base_dir, 'AM{:.1f}/AM*.npy'.format(AM)))
    files.sort(key=get_phase)
    phases = []
    for file in files: #loop through all phases
        print(file)
        data = np.load(file, allow_pickle=True).item()
        freqs, avg_resp = data['psdx'][:, 0], data['psdx'][:, 1]
        _, yavg_resp = data['psdy'][:, 0], data['psdy'][:, 1]
        noise_ranges = np.zeros_like(freqs, dtype=bool)
        for f1, f2 in noise_peaks:
            noise_ranges |= np.logical_and(abs(freqs) > min(f1, f2), abs(freqs) < max(f1, f2))
        freqs = freqs[np.logical_not(noise_ranges)]
        avg_resp = avg_resp[np.logical_not(noise_ranges)]
        yavg_resp = yavg_resp[np.logical_not(noise_ranges)]
        
        yrms = data['yrms_V']
        phase = data['phase']
        
        fwhmi = []
        f0i = []
        integrals = []
        residuals = []
        
        ix = freqs > min(peak_f1, peak_f2)
        ix &= freqs < max(peak_f1, peak_f2)
        
        # avg_resp /= max(avg_resp[ix])
        
        ixb = freqs > min(bf1, bf2)
        ixb &= freqs < max(bf1, bf2)
        

        p0 = {
                'amplitude': max(yavg_resp[ix]),
                # 'center': 860,
                'sigma': 0.2,
                # 'sigma_r': 0.25,
               }

        ax_all[0].semilogy(freqs[ix], avg_resp[ix], color=cmap(abs(phase-180)/180))
        ax_all[1].semilogy(freqs[ix], yavg_resp[ix], color=cmap(abs(phase-180)/180))

        #positive-frequency peak
        Np = sum(ix)
        best_fit_values = []
        
        for k in range(bootstrap_B):
            ixB = rng.choice(np.arange(Np, dtype=int), size=int(0.99*Np), replace=False)
            ixB.sort()
            peak = peak_class(freqs[ix][ixB], yavg_resp[ix][ixB], plot=False, npeaks=1, p0=p0)
            # p0 = peak.out.best_values
            if 'ax' in peak.__dict__:
                peak.ax.set_yscale('log')
                peak.ax.set_title("{}%, {}DEG".format(AM, phase))
            integrals.append(np.trapz(avg_resp[ix], freqs[ix]))
            fwhmi.append(peak.fwhm1)
            f0i.append(peak.f01)
            f01.append(peak.out.best_values['center'])
            best_fit_values.append(peak.out.best_values)
            residuals.append(np.sum(peak.out.residual**2))
        residuals = np.array(residuals)
        
        nmr = np.nanmean(residuals)
        if plot:
            fig, ax = plt.subplots(2, 1, sharex=True)
            ax[0].plot(peak.freq, peak.x)
            amplitude = np.nanmean([abs(v['amplitude']) for v in best_fit_values])#/residuals)*nmr
            center = np.nanmean([abs(v['center']) for v in best_fit_values])#/residuals)*nmr
            sigma = np.nanmean([abs(v['sigma']) for v in best_fit_values])#/residuals)*nmr
            bl_a = np.nanmean([v['bl_intercept'] for v in best_fit_values])#/residuals)*nmr
            bl_b = np.nanmean([v['bl_slope'] for v in best_fit_values])#/residuals)*nmr
            peak = psd_peak.imag_lho(freqs[ix], amplitude, center, sigma) + bl_a + freqs[ix]*bl_b
            ax[0].semilogy(freqs[ix], peak)
            ax[1].plot(freqs[ix], yavg_resp[ix] - peak)
        phases.append(phase)
        yrmss.append(yrms)
        
        integral.append(np.average(integrals, weights=1/residuals))
        integrale.append(np.std(integrals))
        fwhms.append(np.average(fwhmi, weights=1/residuals))
        f0s.append(np.average(f0i, weights=1/residuals))
        fwhmse.append(np.nanstd(fwhmi))
        f0se.append(np.nanstd(f0i))
    # np.save("feedback_peak_parameters/{}_PP_{}_T{:.0f}_AM{:.1f}.npy".format(pressure, D, 1000*T, AM), 
    #         {'fwhms': np.c_[phases, fwhms, fwhmse], 
    #           'f0s': np.c_[phases, f0s, f0se], 
    #           'yrms': np.c_[phases, yrmss], 
    #           'integral' : np.c_[phases, integral, integrale],
    #           'AM': AM})

ax_X.set_xlim(850, 870)
ax_Y.set_xlim(850, 870)
ax_X.set_ylim(1e-7, 2e-1)
ax_Y.set_ylim(1e-6, 5)

xlabel = "frequency (Hz)"
ylabel = "$S_{xx}$ (V$^2$/Hz)"

ax_X.set_xlabel(xlabel)
ax_X.set_ylabel(ylabel)
ax_Y.set_xlabel(xlabel)
ax_Y.set_ylabel(ylabel)

fig_X.tight_layout()
fig_Y.tight_layout()

figure_dir = 'C:/Users/ev/physics/writing/helmholtz-feedback'

fig_X.savefig(path.join(figure_dir, 'PSDsX.pdf'))
fig_Y.savefig(path.join(figure_dir, 'PSDsY.pdf'))