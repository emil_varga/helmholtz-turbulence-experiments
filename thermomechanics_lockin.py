# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 16:15:05 2020

@author: Davis
"""


import numpy as np
import visa
import time
import os
import ltwsclient


import sys
sys.path.append('D:\\ev')
from instruments.DAQcard import DAQcard
from instruments.SR830 import SR830
from instruments.KS33210A import KS33210A

rm = visa.ResourceManager()
lockin = SR830(rm, 'GPIB0::1::INSTR')
gen = KS33210A(rm, '33210A')

timeconstant = '100u'
slope = '6'
sensitivity = '100m'
carrier_frequency = 31e3 #Hz
carrier_amplitude = 20 #Vpp
preamp_gain = 500e-9 #A/V SET BY HAND

gen.frequency(carrier_frequency)
gen.amplitude(carrier_amplitude)
lockin.set_reference('external')
lockin.set_timeconstant(timeconstant)
lockin.set_slope(slope)
lockin.set_sensitivity(sensitivity)
# lockin.set_frequency(carrier_frequency)
# lockin.set_output_amplitude(carrier_amplitude)

print('Connecting to logger.')
server = 'onnes.ccis.ualberta.ca'
port = 3172
lt = ltwsclient.LTWebsockClient(server, port)
print('Connected')

data_dir = '../../data/RUN20210104/dumped/450nm/thermomechanics/lockin/KZM/Tramp/repeated/0.001Kmin'

#DAQ settings
daq_channels = ['ai0', 'ai1']
rate = 8192 #Hz
samples = int(rate*64) #16 s

os.makedirs(data_dir, exist_ok=True)

try:
    #setup DAQ
    daq = DAQcard(daq_channels, rate, samples, min_val=-10, max_val=10)
    while True:
        timestamp = time.strftime('%Y%m%d-%H%M%S')
        try:
            Ti = lt.get_T3()
        except:
            Ti = np.nan
        print(timestamp, end='')
        timeseries = daq.measure()
        print(' ...done')
        try:
            Tf = lt.get_T3()
        except:
            Tf = np.nan
        output = {'timeseries': timeseries, 'rate': rate, 'samples': samples,
                  'Ti': Ti, 'Tf': Tf,
                  'lia_tc': timeconstant, 'lia_slope': slope, 'lia_sens': sensitivity,
                  'carrier_f': carrier_frequency, 'carrier_A': carrier_amplitude,
                  'preamp_gain': preamp_gain,
                  'notes': "using 33210A generator, amplitude Vpp to high-Z load"}
        filename = os.path.join(data_dir, 'TML_{}.npy'.format(timestamp))
        np.save(filename, output, allow_pickle=True)
finally:
    daq.close()