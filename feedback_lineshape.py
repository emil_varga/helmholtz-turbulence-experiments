# -*- coding: utf-8 -*-
"""
Created on Fri Feb 12 15:08:11 2021

@author: ev
"""

import numpy as np
import matplotlib.pyplot as plt

from numpy import sin, cos, sqrt

def a(omega, G, xi, phi, omega0):
    return 2.5e-13*G*xi*omega0*(-omega0*sin(phi) + 194778.58*1j*cos(phi))/(-1.0e-8*omega0**2 + 0.0003*1j*omega0 + 2)

def peak0(omega, omega0, fwhm):
    chi = omega0**2 - omega**2 + 1j*omega*fwhm
    return 1/chi

def peak(omega, omega0, fwhm, G, xi, phi):
    chi0 = peak0(omega, omega0, fwhm)
    return chi0/(1 - a(omegas, G, xi, phi, omegas)*chi0)

def peak1(omega, omega0, fwhm, G, xi, phi):
    chi0 = peak0(omega, omega0, fwhm)
    return chi0/(1 - a(omegas, G, xi, phi, omega0)*chi0)


xi = 1
G = 8e7
omega0 = 1000*2*np.pi
phi = 0
fwhm=1

omegas = np.linspace(omega0-10, omega0+10, 10000)

plt.close('all')
cmap = plt.get_cmap('viridis')
fig, ax = plt.subplots(1, 1)
ax.set_yscale('log')
for phi in [0, 45, 90, 135, 180, 225, 270, 315]:
    resp = peak(omegas, omega0, fwhm, G, xi, phi/180*np.pi)
    ax.plot(omegas, np.abs(resp)**2, label='{} deg'.format(phi), color=cmap(abs(phi-180)/180))
    resp1 = peak1(omegas, omega0, fwhm, G, xi, phi/180*np.pi)
    ax.plot(omegas, np.abs(resp1)**2, label='{} deg'.format(phi), color=cmap(abs(phi-180)/180), ls=':')
ax.legend(loc='best')

ax.plot(omegas, np.abs(peak(omegas, omega0, fwhm, 0, 0, 0))**2, '--', color='r')