# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 14:16:05 2019

@author: Davis
"""

import numpy as np
import matplotlib.pyplot as plt
import os.path as path
from glob import glob

plt.close('all')

data_dir = '../data/RUN_20200926/pump_modes/T1600/ampsweeps/1/10dB'

files = glob(path.join(data_dir, '*.npy'))
files.sort()

print(len(files))


fig, ax = plt.subplots(1,1)
last_down = True
for file in files[:]:
    d = np.load(file, allow_pickle=True).item()
    # if not (d['pump_frequency'] == 2145):
    #     continue
    data = d['data']
    As = data[:,0]
    x = data[:,1]
    y = data[:,2]
    r = np.sqrt(x**2 + y**2)
    if As[0] < As[-1]:
        ls = '-'
        lc = 'b'
        last_down = False
    else:
        ls = '--'
        lc = 'r'
        if last_down:
            lc = 'g'
            ls = '-.'
        last_down = True
    ls += 'o'
    p = np.polyfit(As, r, deg=1)
    ax.plot(As, r/As, ls, color=lc, lw=0.75, ms=1)
