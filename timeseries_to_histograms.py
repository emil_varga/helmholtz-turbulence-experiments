# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 14:45:45 2021

@author: ev
"""

import numpy as np
import scipy.signal as sig

import os
from os import path
from glob import glob
from tqdm import tqdm
import matplotlib.pyplot as plt

from scipy.fft import rfft, rfftfreq, irfft, fft, fftfreq, ifft

from multiprocessing import Pool

plt.close('all')

# D = '450'

base_dir = r'Z:\emil\data\He-4\Helmholtz_turbulence\data\RUN20210104\dumped'
output_base = 'C:/Users/ev/physics/data/RUN20210104/histograms'

feedback_C = 5.403e-9 #F
feedback_Rin = 249+5000 #Ohm
baserate = 8192*2
base_samplet = 16 #s
nperseg = int(base_samplet*baserate)
noverlap = int(0.75*nperseg)

B = 1.5
bins = np.linspace(-B, B, 300)

def feedback_RC(f, R=feedback_Rin, C=feedback_C):
    w = 2*np.pi*f
    nom = 1j*w*R*C + (w*R*C)**2
    denom = 1 + (w*R*C)**2
    return nom/denom

def bandpass(rate, x, fi=700, ff=1000, df=5):
    ft = rfft(x)
    freqs = rfftfreq(len(x), 1/rate)
    band = np.zeros_like(freqs)
    band[abs(freqs) > fi] = np.exp(-(df/(fi - abs(freqs[abs(freqs) > fi])))**2)
    band[abs(freqs) < ff] *= np.exp(-(df/(ff - abs(freqs[abs(freqs) < ff])))**2)
    band[abs(freqs) >= ff] = 0
    # fig, ax = plt.subplots(1, 1)
    # ax.plot(freqs, band, 'o')
    # ax.set_title('band-pass')
    return irfft(ft*band)

def lowpass(rate, x, ff, df=5):
    ft = fft(x)
    freqs = fftfreq(len(x), 1/rate)
    band = np.zeros_like(freqs)
    band[abs(freqs) < ff] = np.exp(-(df/(ff - abs(freqs[abs(freqs) < ff])))**2)
    band[abs(freqs) >= ff] = 0
    # fig, ax = plt.subplots(1, 1)
    # ax.plot(freqs, band, 'o')
    # ax.set_title('low-pass')
    return ifft(ft*band)

def process_deg_dir(DEGdir, Dstr, Tstr, AMstr):
    DEGstr = path.split(DEGdir)[-1][4:-4]
    DEG = float(DEGstr)
    print(Dstr, Tstr, AMstr, DEGstr)
    # output_dir = path.join(output_base, Dstr, 'T'+Tstr, 'AM'+AMstr)
    # output_file = path.join(output_dir, 'AM{}_{}DEG.npy'.format(AMstr, DEGstr))
    # os.makedirs(output_dir, exist_ok=True)
    files = glob(path.join(DEGdir, 'FB*.npy'))
    Hx = 0
    Hy = 0
    fig, ax = plt.subplots(1, 2)
    for file in files[:1]:
        data = np.load(file, allow_pickle=True).item()
        z = data['timeseries'][0, :] + 1j*data['timeseries'][1, :]
        t = np.linspace(0, len(z)*data['rate'], len(z))
        zm = z.mean()
        # z = z/zm*abs(zm)
        z *= np.exp(-1j*DEG/180*np.pi)
        xz, yz = np.real(z), np.imag(z)
        x = bandpass(data['rate'], xz)
        y = bandpass(data['rate'], yz)
        fs, Sxx = sig.periodogram(x + 1j*y, fs=data['rate'])
        f0 = fs[np.argmax(abs(Sxx))]
        print(f0)
        # f0=843.65
        Omegam = abs(f0)*2*np.pi
        # xab = x*np.cos(Omegam*t) + 1j*x*np.sin(Omegam*t)
        # yab = y*np.cos(Omegam*t) + 1j*y*np.sin(Omegam*t)
        # x, y = (x + y), (x-y)
        xfs, xt, Zxx = sig.stft(x, fs=data['rate'], nperseg=1024)
        yfs, yt, Zyy = sig.stft(y, fs=data['rate'], nperseg=1024)
        # # print(fs.shape, t.shape, Zxxshape)
        band = 5
        ix = np.logical_and(xfs > abs(f0)-band, xfs < abs(f0)+band)
        xab = np.trapz(Zxx[ix, :], xfs[ix], axis=0)
        yab = np.trapz(Zyy[ix, :], yfs[ix], axis=0)
        # Omegam = 800
        xab = lowpass(data['rate'], x*np.exp(-1j*Omegam*t), band)
        yab = lowpass(data['rate'], y*np.exp(-1j*Omegam*t), band)
        xab /= np.percentile(abs(xab), 99)
        yab /= np.percentile(abs(yab), 99)
        print(np.mean(abs(xab)), np.std(abs(xab)))

        # print(max(np.real(ab)))
        # print(np.rad2deg(np.angle(np.std(x) + 1j*np.std(y))))
        # print(x.shape, y.shape)
        # ax[1].plot(x[0:20000], y[0:20000], lw=0.1)
        # ax[1].plot(x, y, lw=0.5)
        # M = max(abs(x).max(), abs(y).max())
        # ax[1].set_xlim(-B, B)
        # ax[1].set_ylim(-B, B)
        # ax[1].set_aspect('equal')
        Hxi, _, _ = np.histogram2d(np.real(xab), np.imag(xab), bins=bins)
        Hyi, _, _ = np.histogram2d(np.real(yab), np.imag(yab), bins=bins)
        Hx += Hxi
        Hy += Hyi
        # H += np.abs(Zxx)
    ax[0].set_title('{}, K, AM {}%, {}'.format(Tstr, AMstr, DEGstr))
    ax[0].pcolormesh(bins, bins, np.log(Hx), cmap='inferno')#, vmax=200)
    ax[1].pcolormesh(bins, bins, np.log(Hy), cmap='inferno')#, vmax=200)
    # ax.pcolormesh(t, fs, H, cmap='inferno')
    ax[0].set_aspect('equal')
    ax[1].set_aspect('equal')
       

if __name__ == '__main__':
    Ddirs = glob(path.join(base_dir, '450nm'))
    for Ddir in Ddirs:
        Dstr = path.split(Ddir)[-1]
        Tdirs = glob(path.join(base_dir, '{}/thermomechanics/lockin/feedback/T1000/'.format(Dstr)))
        for Tdir in Tdirs:
            Tstr = path.split(Tdir)[-1][1:]
            AMdirs = glob(path.join(Tdir, 'AM50.0'))
            for AMdir in AMdirs:
                AMstr = path.split(AMdir)[-1][2:]
                DEGdirs = glob(path.join(AMdir, 'PHI_*_DEG'))
                # p = Pool(6)
                for DEGdir in DEGdirs:
                    process_deg_dir(DEGdir, Dstr, Tstr, AMstr)
                # p.close()
                # p.join()
