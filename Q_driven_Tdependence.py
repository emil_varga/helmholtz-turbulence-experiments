# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: emil
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
from fano_peak import SingleFanoPeak
from scipy.ndimage import gaussian_filter

import os
from glob import glob

plt.close('all')

smooth=5

# input_files = glob('grids/grid_AM_1800_50mKmin_5.000.npy')
input_files = glob('grids/grid_TM_450nm_20Vc*.npy')
# input_files += glob('grids/grid_AM_450_50mKmin*.npy')
print(input_files)

for input_file in input_files:
    print("Starting on ", input_file)
    data = np.load(input_file, allow_pickle=True).item()
    pgrid = data['pgrid']
    ngrid = data['ngrid']
    pfreqs = data['pfreqs']
    nfreqs = data['nfreqs']
    Tgs = data['Ts']
    
    REpgrid = gaussian_filter(np.real(pgrid), sigma=(0, smooth))
    IMpgrid = gaussian_filter(np.imag(pgrid), sigma=(0, smooth))
    spgrid = REpgrid + 1j*IMpgrid
    
    REngrid = gaussian_filter(np.real(ngrid), sigma=(0, smooth))
    IMngrid = gaussian_filter(np.imag(ngrid), sigma=(0, smooth))
    sngrid = REngrid + 1j*IMngrid
    
    fig, ax = plt.subplots(1, 2)
    fig.suptitle(input_file)
    ax[0].imshow(np.log(abs(sngrid)), aspect='auto', cmap='inferno')
    ax[1].imshow(np.log(abs(sngrid)), aspect='auto', cmap='inferno')
    
    # Ts = np.linspace(Tgs.min(), Tgs.max), 50)
    Ts = Tgs
    dT = np.mean(np.diff(Ts))
    
    f0s = []
    fwhms = []
    f0es = []
    fwhmes = []
    pp0 = None
    pp0y = None
    np0 = None
    np0y = None
    for T in Ts:
        Tix = abs(T - Tgs) < dT/2
        # print(sum(Tix))
        w = data['samples_per_T'][Tix]
        W = np.nansum(w)
        nresp = np.nansum(sngrid[Tix, :]*w[:, np.newaxis], axis=0)/W
        presp = np.nansum(spgrid[Tix, :]*w[:, np.newaxis], axis=0)/W
        try:
            ppeak = SingleFanoPeak(pfreqs, np.real(presp), p0=pp0, plot=False)
            ppeaky = SingleFanoPeak(pfreqs, np.imag(presp), p0=pp0y, plot=False)
            npeak = SingleFanoPeak(nfreqs, np.real(nresp), p0=np0, plot=False)
            npeaky = SingleFanoPeak(nfreqs, np.imag(nresp), p0=np0y, plot=False)
            pp0 = np.copy(ppeak.popt)
            pp0y = np.copy(ppeaky.popt)
            np0 = np.copy(npeak.popt)
            np0y = np.copy(npeaky.popt)
            f0_fits = [abs(peak.f0) for peak in [ppeak, ppeaky, npeak, npeaky]]
            fwhm_fits = [abs(peak.fwhm) for peak in [ppeak, ppeaky, npeak, npeaky]]
            f0s.append(np.mean(f0_fits))
            f0es.append(np.std(f0_fits))
            fwhms.append(np.mean(fwhm_fits))
            fwhmes.append(np.std(fwhm_fits))
        except:
            fwhms.append(np.nan)
            f0s.append(np.nan)
            fwhmes.append(np.nan)
            f0es.append(np.nan)
            print("Fit failed.")
    
    fig, ax = plt.subplots(1, 1)
    ax.plot(Ts, fwhms,'o')
    ax.set_title('fwhm')
    
    fig, ax = plt.subplots(1, 1)
    ax.plot(Ts, f0s,'o')
    ax.set_title('f0')
    
    np.savetxt(input_file.replace('grid_','').replace('npy', 'txt'), np.c_[Ts, fwhms, f0s, fwhmes, f0es])

