# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: emil
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime
from tqdm import tqdm

import matplotlib.pyplot as plt

import sys
sys.path.append('D:\\ev')
from instruments.SR830 import SR830
from instruments.KS33210A import KS33210A

from ltwsclient import LTWebsockClient

output_dir = '../data/RUN_20210104/testing/helmholtz_modes/1800nm'
plot = True

lockin_sensitivity = '5m'
lockin_timeconstant = '100m'
wait_time = 0.3
auto_adjust_sensitivity=True

freqs = [(1840, 1900, 500)]

amplitudes = [0.02]
print(amplitudes)
reps = 1

os.makedirs(output_dir,exist_ok=True)

rm = visa.ResourceManager()
lockin = SR830(rm, "GPIB0::2::INSTR")
lockin.set_timeconstant(lockin_timeconstant)
lockin.set_sensitivity(lockin_sensitivity)
lockin.set_reference('external')
lockin.set_slope('12')

generator = KS33210A(rm, '33210A')
generator.amplitude(amplitudes[0])
generator.output(True)

if plot:
    # plt.close('all')
    fig, ax = plt.subplots(1, 1)
i = 0
zorder = 0

cmap = plt.get_cmap('viridis')

def animate(i, ax, fs, xs, ys, amp):
    rs = np.sqrt(np.array(xs)**2 + np.array(ys)**2)
    for line in ax.lines:
        line.set_zorder(2)
    if len(ax.lines) > i:
        ax.lines[i].set_xdata(fs)
        ax.lines[i].set_ydata(rs)
        ax.lines[i].set_zorder(5)
    else:
        ax.plot(fs, rs, '-', ms=3, zorder=5)
    ax.relim()
    ax.autoscale_view()
    plt.pause(0.001)

try:
    server = "onnes.ccis.ualberta.ca"
    port = "3172"
    print(f"connecting to {server}:{port}")
    lt = LTWebsockClient(server, port)
    
    while True:
        for r in range(reps):
            sens = lockin_sensitivity
            for A in amplitudes:
                smax = 0
                for fr in freqs:
                    fs = np.linspace(fr[0], fr[1], fr[2])
                    timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                    xs, ys = [], []
                    fs_r = []
                    
                    lockin.set_sensitivity(sens)
                    print("Starting {} Hz, {} V".format(fr, A))
                    try:
                        T3start = lt.get_T3()
                    except:
                        T3start = np.nan
                    lockin.set_output_amplitude(A)
                    # lockin.set_frequency(fs[0]) # set the first frequency
                    generator.amplitude(A)
                    generator.frequency(fs[0])
                    time.sleep(2)
                    
                    for freq in tqdm(fs):
                        # lockin.set_frequency(freq)
                        generator.frequency(freq)
                        time.sleep(wait_time)
                        # fset = lockin.get_frequency()
                        fset = generator.frequency()
                        fset = float(fset)
                        x, y = lockin.get_xy()
        
                        fs_r.append(fset)
                        xs.append(x)
                        ys.append(y)
                        if plot:
                            animate(i, ax, fs_r, xs, ys, A)
                            plt.show()
                        if np.sqrt(x**2 + y**2) > smax:
                            smax = np.sqrt(x**2 + y**2)
                        
                    T3end = lt.get_T3()
                    
                    data = np.column_stack((np.array(fs_r), xs, ys))
                    print(data.shape)
                    out = {'amplitude_Vrms' : A, 'data' : data, 'T3start' : T3start, 'T3end' : T3end,
                           'timeconstant': lockin_timeconstant, 'sensitivity': sens,
                           'wait_time': wait_time}
                    np.save(path.join(output_dir, 'FS_SR_'+timestamp), out, allow_pickle=True)
                    i+=1
                    if i > 9:
                        i=0
                if auto_adjust_sensitivity:
                    sens = lockin.auto_sens(smax)
                    
        # break
finally:
    # lockin.set_output_amplitude(0.01)
    generator.amplitude(0.02)
    generator.output(False)
    lockin.set_output_amplitude(0.01)
    lockin.close()
    generator.close()
    rm.close()