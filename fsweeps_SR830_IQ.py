# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:55:00 2019

@author: emil
"""

import numpy as np
import visa
import os.path as path
import os
import time
from datetime import datetime

import sys
sys.path.append("D:\\ev")
import instruments.SR830 as SR830
from instruments.KS33210A import KS33210A

import ltwsclient as ltwsc
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

T3t = 0.9
T3_tolerance = 0.005
check_T_every = 1 #s


output_dir = '../../data/RUN20210104/1800nm/T900/fsweeps/thermo'

chunk_size = 30
df = 0.1
fmin = 1850
fmax = 1864
wait_time = 1.5 # s

freqs = []
fi = fmin
while fi < fmax:
    freqs.append((fi, fi+chunk_size*df, chunk_size))
    fi += chunk_size*df

# print("Measuring in ranges:")
# for f in freqs:
#     print("\t"+str(f))
    
# drive_amplitudes = [0.05, 0.1, 0.2, 0.5, 1, 2]
# drive_amplitudes = np.linspace(1, 0, 10)
# drive_amplitudes = np.r_[np.linspace(1, 0, 10), np.zeros(10)]
drive_amplitudes = [0]
# drive_amplitudes = [0]
adjust_sensitivity = True

reps = 1000

os.makedirs(output_dir,exist_ok=True)

rm = visa.ResourceManager()

lockin = SR830.SR830(rm, 'GPIB0::2::INSTR')
lockin.clear()

lia_sens = 100e-3
for lia in [lockin]:
    lia.coupling('ac')
    lia.set_sensitivity('100m')
    lia.set_timeconstant('300m')
    lia.set_slope('12')
    lia.set_reference('external')
    lia.set_reserve('normal')
    lia.harmonic(1)

generator = KS33210A(rm, '33210A')
generator.output(False)
generator.amplitude(drive_amplitudes[0])


try:
    r = 0
    while r < reps:
        r += 1
        for amp in drive_amplitudes:
            generator.amplitude(amp)
            set_amp = generator.amplitude()
            n=0
            while n < len(freqs):
                fr = freqs[n]
                fs = np.linspace(fr[0], fr[1], fr[2])
                # np.random.shuffle(fs)
                print(fs[0])
                xs, ys = [], []
                fs_r = []
                ts = []
                T3s = []
                
                while True:
                    try:
                        T3i = lt.get_T3()
                    except:
                        T3i = np.nan
                    if abs(T3i - T3t) < T3_tolerance:
                        print('')
                        break
                    print("Waiting for T3={:.3f} to reach {:.3f}\r".format(T3i, T3t), end='')
                    generator.output(False)
                    unbalanced = True
                    time.sleep(1)
                print("Starting {} Hz, {} V".format(fr,amp))
                               
                #check whether the peak is where we want it
                
                timestamp = datetime.now().strftime('%Y%m%d-%H_%M_%S')
                generator.frequency(fs[0]) # set the first frequency
                fstr = generator.frequency()
                rmax = 0
                t0 = -np.inf
                aborted = False
                nans = 0
                maxnans = 5
                if generator.output(True): #returns true if the output state changed
                    time.sleep(3*wait_time)
                for k in range(len(fs)):
                    t = time.time()
                    if t - t0 > check_T_every:
                        t0 = t
                        try:
                            T3 = lt.get_T3()                                
                        except:
                            T3 = np.nan
                        if np.isnan(T3):
                            nans += 1
                            print("Couldn't get temperature!")
                            if nans > maxnans:
                                print("Too many nans in temperature, aborting.")
                                aborted = True
                                break
                        elif abs(T3 - T3t) > T3_tolerance:
                            print("Lost temperature control, aborting and redoing.")
                            aborted = True
                            break
                        else:
                            nans = 0
                            T3s.append((t, T3))
                            
                    print("\r{}/{}".format(k, len(fs)), end='')
                    freq = fs[k]
                    generator.frequency(freq)
                    time.sleep(wait_time)
                    while True:
                        x, y = lockin.get_xy()
                        r = np.sqrt(x**2 + y**2)
                        if r > rmax:
                            rmax = r
                        if adjust_sensitivity:
                            overload = r > 0.9*lia_sens
                            if overload:
                                print("Lockin overloading!")
                            if rmax < 0.1*lia_sens or overload:
                                print("Changing lockin sensitivity ({:.6f}/{:.6f}), overload={}".format(r, lia_sens, overload))
                                lia_sens = lockin.auto_sens(rmax)
                                time.sleep(0.5)
                                continue
                        break
                    fstr = generator.frequency()
                    fs_r.append(float(fstr))
                    xs.append(x)
                    ys.append(y)
                    ts.append(t)
                
                t = time.time()
                try:
                    T3 = lt.get_T3()
                except:
                    T3 = np.nan
                T3s.append((t, T3))

                if aborted or abs(T3s[-1][1] - T3t) > T3_tolerance:
                    print("Lost temperature control, re-doing.")
                    since_last_vna_check = np.inf
                    generator.output(False)
                    continue
                data = np.column_stack((np.array(fs_r), xs, ys, ts))
                print(data.shape)
                out = {'drive_amplitude_Vpp': set_amp, 'data': data,
                       'temperatures': T3s, 'T3t': T3t}
                np.save(path.join(output_dir, 'FS_SR_'+timestamp), out, allow_pickle=True)
                n+=1
finally:
    lockin.close()
    generator.output(False)
    generator.close()
    rm.close()