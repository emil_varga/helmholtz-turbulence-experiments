# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 11:52:39 2021

@author: Davis
"""


import numpy as np
import matplotlib.pyplot as plt
import os
import time
import visa

import sys
sys.path.append("D:\\ev")
from instruments.DAQcard import DAQcard
from instruments.KS33210A import KS33210A

import ltwsclient as ltwsc
server = "onnes.ccis.ualberta.ca"
port = "3172"
print(f"connecting to {server}:{port}")
lt = ltwsc.LTWebsockClient(server, port)

data_dir = '../../data/RUN20210104/dumped/750nm/driven/DAQ_battery/generator/power_dependence/0'

rm = visa.ResourceManager()
gen = KS33210A(rm, '33210A')

DAQrate = 8192
pulse_t = 32 #s, total duration of the pulse
pulse_tc = 30 #s, total duration of the chirp
pulse_ti = 1 #s, start of the chrip
pulse_fi = 100 #Hz
pulse_ff = 2000 #Hz
pulse_amplitudes = [5, 2, 1, 0.5, 0.2, 0.1, 0.05, 0.02]
# pulse_amplitudes = [1]
# pulse_amplitudes = np.linspace(1, 0, 8)

time_per_power = 1*60*60 #1 hours
max_T = 2.5 #don't measure above this temperature

DAQsamples = int(DAQrate*pulse_t)

ts = np.linspace(0, pulse_t, DAQsamples)

trigger_pulse = np.zeros_like(ts)
ix = np.logical_and(ts > pulse_ti, ts < pulse_ti + 1)
trigger_pulse[ix] = 5

gen.frequency_sweep(True, pulse_fi, pulse_ff, pulse_tc)
gen.output(True)

try:
    while True:
        for pulse_amplitude in pulse_amplitudes:
            gen.amplitude(pulse_amplitude)
            daq = DAQcard(['ai0', 'ai1'], rate=DAQrate, samples=DAQsamples, min_val=-10, max_val=10,
                          outputs=('ao0', trigger_pulse))
            output_dir = os.path.join(data_dir, 'AMP_{:.0f}mV'.format(1000*pulse_amplitude))
            os.makedirs(output_dir, exist_ok=True)
            start_time = time.time()
            while time.time() - start_time < time_per_power:
                Ti = lt.get_T3()
                if Ti > max_T:
                    time.sleep(1)
                    continue
                timestamp = time.strftime('%Y%m%d-%H%M%S')
                data = daq.write_measure()
                Tf = lt.get_T3()
                print(timestamp)
                output = {'timeseries': data[0,:], 'Ti': Ti, 'Tf': Tf, 'pulse': data[1,:],
                          'pulse_amplitude': pulse_amplitude,
                          'samples': DAQsamples, 'rate': DAQrate}
                np.save(os.path.join(output_dir, 'DM_{}.npy'.format(timestamp)), output)
            daq.close()
finally:
    gen.output(False)
    gen.frequency_sweep(False)
    daq.close()        