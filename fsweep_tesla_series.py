#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 17:01:02 2019

@author: emil
"""

import numpy as np
import ziLockin as zi
from datetime import datetime
import os.path as path
import os

device_id = 'dev538'
reps = 2
continuous = True

output_dir = '../data/RUN_20200831/TeslaTest/lowf'
freq_i = 370e3 #Hz
freq_f = 380e3 #Hz
samples = 1000
time_constant = 50e-3
# As_gauge = [1, 0.5, 0.1, 0.05]
# As_pump = np.linspace(0.05, 0.5, 10)
# fs_pump = np.linspace(100, 3000, 10)
As_gauge = [2]
As_pump = np.linspace(0, 0.5, 6)
fs_pump = np.linspace(1, 100, 5)
# As_pump = [0]
# fs_pump = [101]


os.makedirs(output_dir,exist_ok=True)

input_channel  = 0
output_channel = 6
output_id      = 0
osc            = 0
demod_id       = 0
sample_rate    = 200

if __name__ == '__main__':
    print('Connecting to lockin.')
    lockin = zi.ziLockin(device_id)
    
    print('Configuring input.')
    lockin.configure_input(input_channel, input_range = 0.1,
                           ac_coupling=True, imp50 = False,
                           differential=False)
    demods = [0]
    harms = [1]
    
    print('Configuring demodulators.')
    for d, h in zip(demods,harms):
        lockin.configure_demodulator(d, sample_rate, input_channel = input_channel, filter_order=4, 
                                     tc = time_constant, osc=osc, harm=h)
    print('Configuring pump drive.')
    
    not_done = True
    try:
        while not_done:
            for r in range(reps):
                for Agauge in As_gauge:
                    for fpump in fs_pump:
                        for Apump in As_pump:
                            print('Starting {} V, {} Hz (gauge {} V)'.format(Apump, fpump, Agauge))
                            lockin.confgure_oscillator(1, fpump)
                            lockin.configure_output(1, 7, Apump)
                            lockin.output(1, output_on=True, output_range=10, offset=0)
                            now = datetime.now()
                            timestamp = now.strftime('%Y%m%d-%H_%M_%S')
                            sweep_data = lockin.freq_sweep(freq_i, freq_f, samples, Agauge, osc, output_id, output_channel, demods = demods,
                                                           settling_time = 10*time_constant, settling_inaccuracy=10e-3,
                                                           avg_samples = 5, verbose=True, timeout=np.inf,
                                                           scan = zi.scan_sequential)
                            sweep_data['_gauge_drive_amplitude'] = Agauge
                            sweep_data['_pump_drive_amplitude'] = Apump
                            sweep_data['_pump_drive_freq'] = fpump
                            filename = 'FS-'+timestamp
                            np.save(path.join(output_dir, filename), sweep_data)
            if not continuous:
                not_done = False

    finally:
        lockin.disable_everything()