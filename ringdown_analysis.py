# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 09:38:05 2021

@author: ev
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import spectrogram
from numpy.fft import fftshift

import os
from glob import glob

base_dir = r'Z:\emil\data\He-4\Helmholtz_turbulence\data\RUN20210104\dumped'
dataset = r'450nm\driven\AM\power_dependence\ringdown\0.05Kmin\AMD_20.000pc_1.000V'

data_dir = os.path.join(base_dir, dataset)

files = glob(os.path.join(data_dir, '*.npy'))
files.sort()
print(len(files))

plt.close('all')
n = 512
overlap = 1/4
d0 = np.load(files[0], allow_pickle=True).item()
f, t, Pxx = spectrogram(d0['pulse'], fs=d0['rate'], nperseg=n, noverlap=n*overlap,
                        return_onesided=False)

Ts = np.linspace(0.95, 0.53, 40)
waterfall = np.zeros((len(Ts), len(t)))
samples = np.zeros_like(waterfall)
# Ts = []

for file in files[::10]:
    d = np.load(file, allow_pickle=True).item()
    pulse = d['pulse']
    x = d['timeseries'][0,:]
    y = d['timeseries'][1,:]
    T = 0.5*(d['Ti'] + d['Tf'])
    Tm = min(d['Ti'], d['Tf'])
    TM = max(d['Ti'], d['Tf'])
    ix = np.logical_and(Ts > Tm, Ts < TM)
    Nix = sum(ix)
    if Nix == 0:
        ix = np.argmin((Ts-T)**2)
        Nix = 1
    r = abs(x + 1j*y)
    z = x + 1j*y
    f, t, Sxx = sp = spectrogram(z, fs=d['rate'], nperseg=n, noverlap=n*overlap)
    f = fftshift(f)
    Sxx = fftshift(Sxx, axes=0)
    _, _, Pxx = spectrogram(d['pulse'], fs=d['rate'], nperseg=n, noverlap=n*overlap,
                            return_onesided=False)
    Pxx = fftshift(Pxx, axes=0)
    fix = f < -800
    fix &= f > -1200
    E = np.sum(Sxx[fix, :]**2, axis=0)
    Ep = np.sum(Pxx[fix, :]**2, axis=0)
    waterfall[ix,:] += E/Nix
    samples[ix, :] += 1/Nix
    # waterfall.append(E)
    # Ts.append(T)
    fig, ax = plt.subplots(2, 1, sharex=True)
    ax[0].set_title("{:.3f} K".format(T))
    ax[0].pcolormesh(t, f, np.log(Sxx), cmap='inferno')
    ax[1].semilogy(t, E, '-o', ms=3)
    ax[1].semilogy(t, Ep, '-s', ms=3)
    ax[1].set_ylim(ymin = 0.5*E.min())
    # fig.tight_layout()

# Ts = np.array(Ts)
# waterfall = np.array(waterfall)
waterfall /= samples

# ix = np.argsort(Ts)
# Ts = Ts[ix]
# waterfall = waterfall[ix, :]

fig, ax = plt.subplots(1, 1)
ax.pcolormesh(t, Ts, np.log(waterfall))
# ax.imshow(np.log10(waterfall), aspect='auto', vmin=-10)
