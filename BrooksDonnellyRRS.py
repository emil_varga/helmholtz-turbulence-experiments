# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 13:33:01 2021

@author: ev
"""

import numpy as np
import matplotlib.pyplot as plt

import scipy.interpolate as intp

import lmfit

class BrooksDonnellyRRS:
    def __init__(self, calibration_file, P0 = 1.2153, Trange = (0.1, 1.4)):
        Trrs = np.loadtxt('rrs_calibration.txt')
        Trho = np.loadtxt('rho_calibration.txt')
        pressures = [0, 2.53313] #bar
        self.Trrsi = intp.interp2d(pressures, Trrs[:,0], Trrs[:,1:])
        self.Trho = intp.interp2d(pressures, Trho[:, 0], Trho[:, 1:])

    def TP_to_rrhos(self, T, P):
        """
        Calculate the relative rho_s from temperature and pressure.
        """
        return self.Trrsi(P, T)

    def TP_to_rhos(self, T, P):
        """
        Calculate the absolute rho_s from temperature and pressure in kg/m^3
        """
        return self.TP_to_rrhos(T, P)*self.Trho(P, T)*1000