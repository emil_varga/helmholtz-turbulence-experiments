# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 13:33:01 2021

@author: ev
"""

import numpy as np
import matplotlib.pyplot as plt

import scipy.interpolate as intp

import lmfit

class HelmholtzF0Calibration:
    def __init__(self, calibration_file, P0 = 1.2153, Trange = (0.1, 1.4)):
        self.cal = np.load(calibration_file)
        Trrs = np.loadtxt('rrs_calibration.txt')
        Trrs_pressures = [0, 2.53313] #bar
        self.Trrsi = intp.interp2d(Trrs_pressures, Trrs[:,0], Trrs[:,1:])
        self.Vcpi = intp.interp1d(self.cal[:,0]**2, self.cal[:,1:], axis=0)
        self.Ts = np.linspace(min(Trange), max(Trange), 100)
        self.Trrs = self.Trrsi(P0, self.Ts)[:,0]
        self.rrsTi = intp.interp1d(self.Trrs, self.Ts, bounds_error=False, fill_value=np.nan)
    
    def f0_to_T(self, f0, Vc, AM, yrmsp):
        Vceff = Vc*np.sqrt(1 + AM**2/4*yrmsp**2)
        p = self.Vcpi(Vceff**2)
        rrs_f0 = ((f0 - p[:, 1])/p[:, 0])**2
        return self.rrsTi(rrs_f0)

if __name__ == '__main__':
    plt.close('all')
    Trrs = np.loadtxt('rrs_calibration.txt')
    Trrs_pressures = [0, 2.53313] #bar
    Trrsi = intp.interp2d(Trrs_pressures, Trrs[:,0], Trrs[:,1:])
    
    fig, ax = plt.subplots(1, 1)
    
    ax.plot(Trrs[:, 0], Trrs[:, 1], 'o', ms=3)
    ax.plot(Trrs[:, 0], Trrs[:, 2], 'o', ms=3)
    ax.plot(Trrs[:, 0], Trrsi(0, Trrs[:,0]), '-')
    ax.plot(Trrs[:, 0], Trrsi(2.53313, Trrs[:,0]), '-')
    ax.plot(Trrs[:, 0], Trrsi(1.2153, Trrs[:,0]), '-')
    
    P0 = 1.2153
    calibration_Vcs = np.array([8, 10, 12, 14, 16, 18, 20])
    
    model = lmfit.models.LinearModel()
    
    def cal_curve(calibration, Tmax=1.3, ax=None):
        T = calibration[:, 0]
        f = calibration[:, 2]
        ix = T < Tmax
        rrs = Trrsi(P0, T)[:,0]
        # p = np.polyfit(np.sqrt(rrs[ix]), f[ix], deg=1)
        params = model.make_params()
        params['slope'] = lmfit.Parameter('slope', value=1500)
        params['intercept'] = lmfit.Parameter('intercept', value=-642.3216238254839, vary=False)
        out = model.fit(f[ix], params, x=np.sqrt(rrs[ix]))
        
        if ax is not None:
            pl = ax.plot(T, f, 'o', ms=3)
            Tplot = np.linspace(0, 1.5, 100)
            rrsplot = Trrsi(P0, Tplot)
            ax.plot(Tplot, model.eval(params=out.params, x=np.sqrt(rrsplot)), color=pl[0].get_color())
        return [out.best_values['slope'], out.best_values['intercept']]
    
    fig, ax = plt.subplots(1, 1)
    
    ps = []
    for Vc in calibration_Vcs:
        calibration_file = 'grids/TM_450nm_NM_{}Vc.txt'.format(Vc)
        calibration = np.loadtxt(calibration_file)
        ps.append(cal_curve(calibration, Tmax=1.3, ax=ax))
    
    ps = np.array(ps)
    
    fig, ax = plt.subplots(2, 1)
    ax[0].plot(calibration_Vcs**2, ps[:, 0], '-o')
    ax[1].plot(calibration_Vcs**2, ps[:, 1], '-o')
    
    np.save("TVcf0_450nm_calibration.npy", np.c_[calibration_Vcs, ps])
    HelmholtzF0Calibration("TVcf0_450nm_calibration.npy")