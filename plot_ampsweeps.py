# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 18:39:38 2019

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import os.path as path
from glob import glob

plt.close('all')

def get_amplitude(Ffile):
    with open(Ffile,'r') as data:
        lines = data.readlines()
        for line in lines:
            if not line.find('amplitude') < 0:
                return float(line.split('=')[1])

#data_dir = '../data/RUN_20200304/Zurich/film/T1350/dirty/ampsweeps1'
#data_dir = '../data/RUN_20200304/Zurich/film/T1350/10mKmin_1/ampsweeps1'
#data_dir = '../data/RUN_20200304/Zurich/film/T1350/fast1/ampsweeps1'
data_dir = '../data/RUN_20200712/lockin_ampsweeps/UM/T1000/'

files = glob(path.join(data_dir, '*.npy'))# + glob(path.join(data_dir2, '*.npy'))
files.sort()

fig, ax = plt.subplots(1,1)

cmap = plt.get_cmap('tab10')
colors = {100:cmap(0), 200:cmap(1), 500:cmap(2),
          1000:cmap(3), 2000:cmap(4), 5000:cmap(5)}

T3max = 1.025
exp=0
offset = 0.000
img = []
for k, file in enumerate(files[:]):
    data = np.load(file)
    if data.item()['_Tstart'] > T3max or data.item()['_Tend'] > T3max:
        continue
    if len(data.item()['/dev538/demods/0/sample']) < 2:
        downsweep = data.item()['/dev538/demods/0/sample'][0][0]
        print(downsweep['samplecount'])
        Vdw = downsweep['grid']
        Idw = downsweep['r']
        ax.plot(Vdw, Idw/Vdw**exp + k*offset, ':', color='g')
        img.append(Idw/Vdw)
        continue
    
    upsweep = data.item()['/dev538/demods/0/sample'][0][0]
    downsweep = data.item()['/dev538/demods/0/sample'][1][0]
    print(upsweep['samplecount'])
    sc = upsweep['samplecount']
    
    Vup = upsweep['grid']
    Iup = upsweep['r']
    Vdw = downsweep['grid']
    Idw = downsweep['r']
    
#    if max(Vup) < 0.5:
#        Vup *= 10
#        Vdw *= 10
#        ax.plot(Vup, Iup/Vup**exp, 'k-')#, color=colors[sc])
#        ax.plot(Vdw, Idw/Vdw**exp, 'c--')#, color=colors[sc])
#        continue
    
    ax.plot(Vup, Iup/Vup**exp + k*offset, 'b-o', ms=3)#, color=colors[sc])
    ax.plot(Vdw, Idw/Vdw**exp + k*offset, 'r--o', ms=3)#, color=colors[sc])

print(len(files))

#img = np.array(img)
#print(img.shape)
#fig, ax = plt.subplots(1,1)
##ax.imshow(np.log(img), aspect='auto')
#ax.imshow((img), aspect='auto', interpolation='nearest')

#ax.set_yscale('log')
#ax.set_xscale('log')
    
#    ixl = Vup < 0.06
#    ixt = Vup > 0.2
#    p_laminar = np.polyfit(Iup[ixl], Vup[ixl], deg=1)
#    p_turbulent = np.polyfit(Iup[ixt], Vup[ixt], deg=2)
#    
#    ax.plot(Iup, np.polyval(p_laminar, Iup), color='0.5')
#    ax.plot(Iup, np.polyval(p_turbulent, Iup), color='0.5')
#    