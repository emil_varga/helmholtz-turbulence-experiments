# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 11:07:05 2020

@author: emil
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig

import os
from glob import glob

plt.close('all')

# rates = ['0.002', '0.05', '0.1', '0.5', '1']
rates = ['0.02']
Tt = 0.8
Ttol = 0.002

for r in rates:
    Trate = '{}Kmin'.format(r)
    print(Trate)
    data_dir = r'D:/ev/data/RUN20210104/dumped/450nm/thermomechanics/lockin/KZM/Tramp/repeated/{}'.format(Trate)
    # data_dir = r'D:/ev/data/RUN20210104/750nm/thermomechanics/lockin/KZM/10mKmin'
    
    files = glob(os.path.join(data_dir, '*.npy'))
    files.sort()
    print(len(files))
    
    nscale=1
    
    data0 = np.load(files[0], allow_pickle=True).item()
    z = data0['timeseries'][0,:] + 1j*data0['timeseries'][1,:]
    nperseg = data0['samples']/4
    noverlap = None#int(nperseg/2)
    nfft = None#data0['samples']
    freqs, _= sig.welch(z, fs=data0['rate'],
                        nperseg=nperseg, noverlap=noverlap, nfft=nfft, return_onesided=False)
    
    psd = 0
    samples = 0
    for file in files:
        data = np.load(file, allow_pickle=True).item()
        Ti = data['Ti']
        Tf = data['Tf']
        if (max(Ti, Tf) > Tt + Ttol) or (min(Ti, Tf) < Tt - Ttol):
            continue
        z = data['timeseries'][0,:] + 1j*data['timeseries'][1,:]
        _, resp = sig.welch(z, nperseg=nperseg, noverlap=noverlap, nfft=nfft, return_onesided=False)
        # _, resp = sig.periodogram(z, fs=data['rate'])
        psd += resp
        samples += 1
    print(samples)
    psd /= samples
    fig, ax = plt.subplots(1, 1)
    ax.plot(freqs, psd, '-o')
    ax.set_title(r)
    fig.tight_layout()
    np.savetxt("rKZM{}_{:.0f}mK.txt".format(Trate, Tt*1000), np.c_[freqs, psd])
