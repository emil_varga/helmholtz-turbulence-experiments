"""
Fitting of peaks with complex coherent backgrounds that could result in fano-like shapes.

Created on Wed Jul 24 08:56:15 2019

@author: emil
"""

import numpy as np
import scipy.optimize as optim
import matplotlib.pyplot as plt
import scipy.interpolate as intp
import lmfit

rng = np.random.default_rng()

Omegac0 = 31e3 #Hz
tauH0 = 27e-6 #s
tauL0 = 100e-6

def filter_resp(omega, tauH=tauH0, tauL=tauH0):
    x = (1 + 1j*omega*tauH)*(1 + 1j*omega*tauL)
    return 1/x

def phase_dep(omega, phi, Omegac=Omegac0):
    return 1j*Omegac*omega*np.cos(phi) - omega**2*np.sin(phi)

def feedback_gain(omega, phi, g0, Omegac=Omegac0, tauH=tauH0, tauL=tauL0):
    return 0.5*g0*filter_resp(omega, tauH, tauL)*phase_dep(omega, phi, Omegac)
 
def feedback_imag_lho(x, amplitude, center, sigma, c):
    
    Z = x**2 - center**2 - 1j*x*sigma
    return np.imag(amplitude/Z)/x*sigma*center**2 + c

def imag_lho(x, amplitude, center, sigma, c):
    Z = x**2 - center**2 - 1j*x*sigma
    return np.imag(amplitude/Z)/x*sigma*center**2 + c

def split_imag_lho(x, amplitude, center, sigma, sigma_r, c):
    Z = x**2 - center**2 - 1j*x*sigma
    peak_l = np.imag(amplitude/Z)/x*sigma*center**2
    
    Z_r = x**2 - center**2 - 1j*x*sigma_r
    peak_r = np.imag(amplitude/Z_r)/x*sigma_r*center**2
    
    return np.heaviside(center - x, 0.5)*peak_l + np.heaviside(x - center, 0.5)*peak_r + c


class FeedbackPeak:
    """Fit a single peak with rotated phase."""

    def __init__(self, freq, x, plot=False, p0=None, plot_failed=True, y=None,
                 f0=None, npeaks=2, upsamples=0, f0hints=None):
        self.plot_failed = plot_failed
        self.f0hints=None
        self.npeaks = npeaks
        self.freq = freq
        self.x = x
        self.plot = plot
        self.p0 = p0
        self.f0 = f0
        if upsamples > 1:
            pi = intp.interp1d(freq, np.log(x), kind='cubic')
            self.freq = np.linspace(self.freq.min(), self.freq.max(), upsamples*len(self.freq))
            self.x = np.exp(pi(self.freq))
        self.fit()

    def fit(self):
        """Fit the data to a single peak with rotated phase."""
        ix0 = np.argmax(np.abs(self.x))
        A0 = np.abs(self.x[ix0])/5
        if self.f0 is not None:
            f0 = self.f0
        else:
            f0 = self.freq[ix0]
        s = np.sign(f0)
        bg = 0
        fwhm_ix = self.x > 0.5*(bg + A0)
        fwhm = max(max(self.freq[fwhm_ix]) - min(self.freq[fwhm_ix]),
                   0)
        q0 = 1e4
        # A0 /= fwhm**2*q0**2

        model = lmfit.Model(split_imag_lho)
        # model += lmfit.models.GaussianModel(prefix='b_')
        # model = lmfit.models.SplitLorentzianModel()
        
        # model += lmfit.models.ConstantModel()
        pars = model.make_params(sigma=fwhm/4, center=f0, amplitude=A0, q=0,
                                 a=0, b=0, c=self.x.min(), sigma_r=fwhm/4,
                                 b_amplitude=1e-5, b_center=f0, b_sigma=10)
        pars['c'].set(min=0, vary=False)
        # pars['b_amplitude'].set(1.5e-5, vary=False)
        # pars['b_center'].set(865, vary=False)
        # pars['b_sigma'].set(10, vary=False)
        # pars['b_amplitude'].set(min=1e-7)
        # pars['sigma'].set(min=0)
        # pars['fraction'].set(min=0, max=1)
        # pars['amplitude'].set(min=0)

        # pars['center'] = lmfit.Parameter('center', f0, min=fmin, max=fmax)
        # pars['br_amplitude'] = lmfit.Parameter('br_amplitude', 5, min=0)
        # pars['br_center'] = lmfit.Parameter('br_center', s*875, vary=False)
        # pars['bl_amplitude'] = lmfit.Parameter('bl_amplitude', 5, min=0)
        # pars['bl_center'] = lmfit.Parameter('bl_center', s*859, vary=False)
        for k in range(self.npeaks-1):
            prefix='b{}_'.format(k)
            # model += lmfit.Model(log_pvoigt, prefix='b{}_'.format(k))
            model += lmfit.models.LorentzianModel(prefix='b{}_'.format(k))
            # pars[prefix+'fraction'] = lmfit.Parameter(prefix+'fraction', 0.5, min=0, max=1)
            # pars[prefix+'a'] = lmfit.Parameter(prefix+'a', 0.5, min=0, max=1)
            pars[prefix+'sigma'] = lmfit.Parameter(prefix+'sigma', 3)#, min=1, max=5)
            if self.f0hints is not None:
                bf0 = self.f0hints[k]
            else:
                bf0 = rng.choice(self.freq)
            ba0 = self.x[np.abs(self.freq - bf0) < 3].mean()
            pars[prefix+'center'] = lmfit.Parameter(prefix+'center', bf0)
            pars[prefix+'amplitude'] = lmfit.Parameter(prefix+'amplitude', ba0)
        
        if self.p0 is not None:
            for par in self.p0:
                pars[par].set(self.p0[par])
        
        # pars += peak.guess(data=self.x, x=self.freq)
        out = model.fit((self.x), pars, x=self.freq, nan_policy='propagate')#, weights=1/(self.x.mean()+self.x))
        self.out = out
        # print(out.fit_report())
        
        self.f0 = abs(out.best_values['center'])
        self.fwhm = 0.5*abs(out.best_values['sigma'] + abs(out.best_values['sigma_r']))
        self.Q = self.f0/self.fwhm
        self.A0 = out.best_values['amplitude']/(2*self.fwhm*self.f0**2)
        
        self.f01 = self.f0
        self.fwhm1 = self.fwhm
        self.Q1 = self.Q
        self.Q2 = self.Q
        
        self.popt = [self.A0, self.f0, self.fwhm]

        if self.plot:
            fig, ax = plt.subplots(1, 1)
            self.fig = fig
            self.ax = ax
            ax.plot(self.freq, (self.x), 'o')
            ax.plot(self.freq, (out.init_fit), ':', label='initial')
            ax.plot(self.freq, (out.best_fit), '-', label='best fit')
            if len(out.components) > 1:
                for c in self.out.components:
                    comp_y = np.atleast_1d(c.eval(params=self.out.params, x=self.freq))
                    if len(comp_y) == len(self.freq):
                        ax.plot(self.freq, c.eval(params=self.out.params, x=self.freq), '--', label=c._name)
                    else:
                        ax.axhline(comp_y, ls='--', label=c._name)
            ax.legend(loc='best')
