"""
Fitting of peaks with complex coherent backgrounds that could result in fano-like shapes.

Created on Wed Jul 24 08:56:15 2019

@author: emil
"""

import numpy as np
import scipy.optimize as optim
import matplotlib.pyplot as plt
import scipy.interpolate as intp
import lmfit

rng = np.random.default_rng()

def imag_lho(x, amplitude, center, sigma):
    Z = x**2 - center**2 - 1j*x*sigma
    return np.imag(amplitude/Z)*sigma*center

def split_imag_lho(x, amplitude, center, sigma, sigma_r, c):
    Z = x**2 - center**2 - 1j*x*sigma
    peak_l = np.imag(amplitude/Z)/x*sigma*center**2
    
    Z_r = x**2 - center**2 - 1j*x*sigma_r
    peak_r = np.imag(amplitude/Z_r)/x*sigma_r*center**2
    
    return np.heaviside(center - x, 0.5)*peak_l + np.heaviside(x - center, 0.5)*peak_r + c
    

def complex_lho(x, amplitude, center, sigma, q):
    Z = (x**2 - center**2 - 1j*x*sigma)*np.exp(-1j*q)
    return np.imag(amplitude/Z)/x*sigma*center**2

def log_imag_lho(x, amplitude, center, sigma):
    Z = x**2 - center**2 - 1j*x*sigma
    return np.log(np.imag(amplitude/Z)/x*sigma*center**2)

def log_pvoigt(x, amplitude, center, sigma, a):
    sigmag = sigma/2/np.log(2)
    gauss = (1-a)/sigmag/np.sqrt(2*np.pi)*np.exp(-(x**2 - center**2)/2/sigmag**2)
    lho = imag_lho(x, a, center, sigma)
    return np.log(amplitude*(gauss + lho))

class SingleFanoPeak:
    """Fit a single peak with rotated phase."""

    def __init__(self, freq, x, plot=False, p0=None, plot_failed=True, y=None,
                 f0=None, npeaks=2, upsamples=0, f0hints=None):
        self.plot_failed = plot_failed
        self.f0hints=None
        self.npeaks = npeaks
        self.freq = freq
        self.x = x
        self.plot = plot
        self.p0 = p0
        self.f0 = f0
        if upsamples > 1:
            pi = intp.interp1d(freq, np.log(x), kind='cubic')
            self.freq = np.linspace(self.freq.min(), self.freq.max(), upsamples*len(self.freq))
            self.x = np.exp(pi(self.freq))
        self.fit()

    def fit(self):
        """Fit the data to a single peak with rotated phase."""
        ix0 = np.argmax(np.abs(self.x))
        A0 = np.abs(self.x[ix0])/5
        if self.f0 is not None:
            f0 = self.f0
        else:
            f0 = self.freq[ix0]
        bg = 0
        fwhm_ix = self.x > 0.5*(bg + A0)
        fwhm = max(max(self.freq[fwhm_ix]) - min(self.freq[fwhm_ix]),
                   0)

        model = lmfit.Model(imag_lho)
        # model += lmfit.models.ConstantModel(prefix='bc_')
        model += lmfit.models.LinearModel(prefix='bl_')
        # model += lmfit.models.QuadraticModel(prefix='bq_')
        # model += lmfit.models.GaussianModel(prefix='bg_')
        # model = lmfit.models.SplitLorentzianModel()
        
        pars = model.make_params(sigma=fwhm/4, center=f0, amplitude=A0)
        pars['amplitude'].set(min=0)
        pars.add('bg_l', value=0, min=0)
        pars.add('bg_r', value=0, min=0)
        DF = self.freq.max() - self.freq.min()
        pars.add('bl_intercept', expr='bg_l - (bg_r-bg_l)/{}'.format(DF))
        pars.add('bl_slope', expr='(bg_r - bg_l)/{}'.format(DF))

        for k in range(self.npeaks-1):
            prefix='b{}_'.format(k)
            # model += lmfit.Model(log_pvoigt, prefix='b{}_'.format(k))
            model += lmfit.models.LorentzianModel(prefix='b{}_'.format(k))
            # pars[prefix+'fraction'] = lmfit.Parameter(prefix+'fraction', 0.5, min=0, max=1)
            # pars[prefix+'a'] = lmfit.Parameter(prefix+'a', 0.5, min=0, max=1)
            pars[prefix+'sigma'] = lmfit.Parameter(prefix+'sigma', 3)#, min=1, max=5)
            if self.f0hints is not None:
                bf0 = self.f0hints[k]
            else:
                bf0 = rng.choice(self.freq)
            ba0 = self.x[np.abs(self.freq - bf0) < 3].mean()
            pars[prefix+'center'] = lmfit.Parameter(prefix+'center', bf0)
            pars[prefix+'amplitude'] = lmfit.Parameter(prefix+'amplitude', ba0)
        
        if self.p0 is not None:
            for par in self.p0:
                pars[par].set(self.p0[par])
        
        # pars += peak.guess(data=self.x, x=self.freq)
        out = model.fit((self.x), pars, x=self.freq, nan_policy='propagate')#, weights=1/(self.x.mean()+self.x))
        self.out = out
        # print(out.fit_report())
        
        self.f0 = abs(out.best_values['center'])
        # self.fwhm = 0.5*abs(out.best_values['sigma'] + abs(out.best_values['sigma_r']))
        self.fwhm = abs(out.best_values['sigma'])
        self.Q = self.f0/self.fwhm
        self.A0 = out.best_values['amplitude']/(2*self.fwhm*self.f0**2)
        
        self.f01 = self.f0
        self.fwhm1 = self.fwhm
        self.Q1 = self.Q
        self.Q2 = self.Q
        
        self.popt = [self.A0, self.f0, self.fwhm]

        if self.plot:
            fig, ax = plt.subplots(1, 1)
            self.fig = fig
            self.ax = ax
            ax.plot(self.freq, (self.x), 'o')
            ax.plot(self.freq, (out.init_fit), ':', label='initial')
            ax.plot(self.freq, (out.best_fit), '-', label='best fit')
            if len(out.components) > 1:
                for c in self.out.components:
                    comp_y = np.atleast_1d(c.eval(params=self.out.params, x=self.freq))
                    if len(comp_y) == len(self.freq):
                        ax.plot(self.freq, c.eval(params=self.out.params, x=self.freq), '--', label=c._name)
                    else:
                        ax.axhline(comp_y, ls='--', label=c._name)
            ax.legend(loc='best')
