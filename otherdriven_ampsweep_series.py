#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 17:01:02 2019

This will drive the peak 1 at 1 V to ensure it is in turbulent state
and sweep the voltage on the other to check whether the multiple
transitions are due to the turbulence in the other device.

@author: emil
"""

import numpy as np
from ziLockin import ziLockin
from datetime import datetime
import os.path as path
import os

device_id = 'dev538'
output_dir = '../data/T1p65/ampsweeps/otherdriven'
os.makedirs(output_dir,exist_ok=True)

Ai = 0
Af = 1.5
#sampless = [100]
#reps = 1
#samples = 100
sampless = [1000, 2000]
reps = 5

#1.375 K
#f1 = 1171 #Hz
#f2 = 1421.5 #Hz

#1.65 K
f1 = 1108 #Hz
f2 = 1350 #Hz

#1.95 K
#f1 = 965 #Hz
#f2 = 1178 #Hz

fs = [f2]

input_channel  = 0
output_channel = 6
output_id      = 0
osc            = 0
demod_id       = 0
sample_rate    = 300
time_constant  = 30e-3
#harmonic       = 1

lockin = ziLockin(device_id)

lockin.configure_input(input_channel, input_range = 1, 
                       ac_coupling=True, imp50 = False,
                       differential=False)
demods = [0,1,2,3,4,5]
harms = [1,2,3,4,6,8]

for d, h in zip(demods,harms):
    lockin.configure_demodulator(d, sample_rate, input_channel = input_channel, filter_order=4, 
                                 tc = time_constant, osc=osc, harm=h)

lockin.confgure_oscillator(1, f1)
lockin.output(0, output_on = False)
lockin.output(0, output_range=10)
lockin.configure_output(0, 7, 1/10, enable=True)
lockin.configure_output(0, 6, 0, enable=True)
lockin.output(0, output_on = True)

for rep in range(reps):
    for samples in sampless:
        for f in fs:
            now = datetime.now()
            
            sweep_data = lockin.amp_sweep(Ai, Af, samples, f, osc, output_id, output_channel, demods = demods,
                                          timeout = np.inf,verbose=True,
                                          dont_change_output_state = True,
                                          ramp_up = False)
            
            timestamp = now.strftime('%Y%m%d-%H_%M_%S')
            np.save(path.join(output_dir, 'AS-'+timestamp), sweep_data)